/** Angular core dependencies */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
/** Custom Components */
import {LoadingPlaceholderComponent} from './loading-placeholder/loading-placeholder.component';
import {SpinnerComponent} from './spinner/spinner.component';


/** Third party modules */

// tslint:disable-next-line:jsdoc-format
/** Custom Components Registration*/
export const COMPONENTS = [
    LoadingPlaceholderComponent,
    SpinnerComponent
];

@NgModule({
    imports: [
        /** Angular core dependencies */
        CommonModule,
        RouterModule,
        /** Third party modules */

    ],
    declarations: COMPONENTS,
    exports: COMPONENTS
})
export class ComponentsModule {
}
