export interface Cmt {
    id: number;
    commentContent: string;
    user_id: string;
    username: string;
    article_id: string;
    created_at: string;
    updated_at: string;
}
