export interface ActiveStatus {
    id: number;
    activeName: string;
}

export interface PublishStatus {
    id: number;
    publishName: string;
}
