export interface Class {
    id: number;
    courseName: string;
    department_id: number;
    departmentName: string;
    created_at: string;
    updated_at: string;
}

export interface ClassResolved {
    classes: Class[];
    error?: any;
}

export interface ClassEditResolved {
    class: Class;
    error?: any;
}

