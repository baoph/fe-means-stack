export interface Message {
    _id?: string;
    message: string;
    createdAt?: string;
    chat: string;
    sender: string;
    receiver: string;
}


