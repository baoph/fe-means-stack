export interface Order {
  _id: string;
  orderDate: string;
  orderTime: string;
  adult: number;
  children: number;
  user: string;
  username: string;
  email: string;
  tel: string;
  note: string;
  restaurant: string;
}

export interface OrderResolved {
  order: Order[];
  error?: any;
}

export interface OrderEditResolved {
  order: Order;
  error?: any;
}


