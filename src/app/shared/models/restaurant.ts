import {Article} from './article';
import {ApiResponse} from './response';

export interface Restaurant {
  _id: string;
  username: string;
  tel: string;
  email: string;
  resName: string;
  resAddress: string;
  article: Article;
}

// export interface ArticleFiles {
//   id: number;
//   article_id: number;
//   title: string;
//   file_path: string;
//   type: number;
//   created_at: string;
//   updated_at: string;
//   deleted_at: string;
// }

export interface RestaurantResolved {
  restaurants: ApiResponse;
  error?: any;
}

export interface RestaurantEditResolved {
  restaurant: Restaurant;
  error?: any;
}

export interface ChangeStatusArticle {
  restaurant_id: number;
  restaurantStatus: number;
}

