export interface Role {
  id: number;
  roleName: string;
}

export interface RoleResolved {
  article: Role[];
  error?: any;
}

export interface RoleEditResolved {
  article: Role;
  error?: any;
}

