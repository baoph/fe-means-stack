export interface Topic {
  id: number;
  topicName: string;
}

export interface TopicResolved {
  topic: Topic[];
  error?: any;
}

export interface TopicEditResolved {
  topic: Topic;
  error?: any;
}

