export interface Department {
    id: number;
    departmentName: string;
    created_at: string;
    updated_at: string;
}

export interface DepartmentResolved {
    departments: Department[];
    error?: any;
}

export interface DepartmentEditResolved {
    department: Department;
    error?: any;
}

