import {Article} from './article';

export class LinkJSON {
  first: string;
  last: string;
  prev: string;
  next: string;
}

export class MetaJSON {
    // tslint:disable-next-line:variable-name
  current_page: number;
  from: number;
    // tslint:disable-next-line:variable-name
  last_page: number;
  path: string;
    // tslint:disable-next-line:variable-name
  per_page: number;
  to: number;
  total: number;
}

export class ActionResponse {
  error: boolean;
  code: number;
  message: string;
  callback?: any;
}

// export class UserAuthenticationResponse {
//   status?: string;
//   error?: boolean;
//   access_token?: string;
//   token_type?: string;
//   expires_in?: number;
//   user?: UserRouting;
//   message?: string;
// }

export class ArticleResponse {
  data: Article;
}

export class ArticleCollectionResponse {
  data: Article[];
  links?: LinkJSON;
  meta?: MetaJSON;
}


export interface ArticleCollectionResolved {
    articlesResponse: ArticleCollectionResponse;
    error?: any;
}
