export interface Room {
    id: number;
    roomName: string;
}

export interface RoomResolved {
    rooms: Room[];
    error?: any;
}

export interface RoleEditResolved {
    room: Room;
    error?: any;
}

