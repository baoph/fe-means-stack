export interface Subject {
    id: number;
    subjectName: string;
    session: number;
    course_id: number;
    teacher_id: number;
    teacherName: string;
    created_at: string;
    updated_at: string;
}

export interface SubjectResolved {
    subjects: Subject[];
    error?: any;
}

export interface SubjectEditResolved {
    subject: Subject;
    error?: any;
}

