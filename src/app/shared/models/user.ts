export interface User {
  email: string;
  password: string;
}

export interface LoggedInUser {
  status: number;
  _id: string;
  role: string;
  email: string;
  tel: string;
  username: string;
}

export interface UserInfo {
  id: string;
  email: string;
  role: number;
}
