export interface Slot {
    title: string;
    start_time: string;
    end_time: string;
}
