export interface IErrorResponse {
  errors: IApiError[];
}

export interface IApiError {
  message: string;
  statusCode?: number;
  error?: string;
  body?: any;
}
