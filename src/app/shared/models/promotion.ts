import {Restaurant} from './restaurant';
import {ApiResponse} from './response';

export interface Promotion {
  _id: string;
  promotionName: string;
  promotionPercent: string;
  promotionFrom: string;
  promotionTo: string;
  restaurant: Restaurant;
}

export interface PromotionResolved {
  promotion: ApiResponse;
  error?: string;
}
