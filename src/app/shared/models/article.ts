import {Category} from './category';

export interface Article {
  _id: string;
  articleContent: string;
  articleImage: string;
  priceRange: string;
  category: Category;
}

export interface ArticleResolved {
  article: Article[];
  error?: any;
}

export interface ArticleEditResolved {
  article: Article;
  error?: any;
}


