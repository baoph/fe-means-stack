export interface Schedule {
    id: number;
    start_date: string;
    end_date: string;
    subject_id: number;
    subjectName: string;
    room_id: number;
    roomName: string;
    created_by: number;
    createdName: string;
    course_id: number;
    courseName: string;
    teacher_id: number;
    teacherName: string;
    created_at: string;
    updated_at: string;
}

export interface CourseScheduleResolved {
    schedules: Schedule[];
    error?: any;
}

export interface StudentScheduleEditResolved {
    schedules: Schedule[];
    error?: any;
}

export interface TeacherScheduleEditResolved {
    schedules: Schedule[];
    error?: any;
}

