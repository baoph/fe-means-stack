import {NgModule} from '@angular/core';
import {AlertDialogComponent} from './alert-dialog.component';
import {MaterialModule} from '../material/material.module';


@NgModule({
  declarations: [
    AlertDialogComponent
  ],
  imports: [
    MaterialModule
  ],
  providers: [],
})
export class AlertModule {}
