import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoreComponent} from './core.component';

import {FooterComponent} from './footer/footer.component';
// import {MaterialModule} from '../../../material/material.module';
import {RouterModule} from '@angular/router';
import {HeaderComponent} from './header/header.component';

const CONTAINERS = [
    CoreComponent,
    HeaderComponent,
    FooterComponent,
];

@NgModule({
    declarations: [
        CONTAINERS
    ],
    exports: [
        CONTAINERS
    ],
    imports: [
        /** Angular core dependencies */
        CommonModule,
        // MaterialModule,
        RouterModule,
    ]
})
export class CoreLayoutModule {
}
