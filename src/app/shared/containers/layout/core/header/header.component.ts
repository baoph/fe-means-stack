import {AfterViewInit, Component, OnInit} from '@angular/core';
// import {AuthService} from '../../../../services/http/auth.service';
import {AuthService} from '../../../../services/http/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
  isAdmin = false;
  isManager = false;
  isStudent = false;
  isTeacher = false;
  role: number;
  email: string;
  userId: string;
  username: string;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.username = this.authService.getLoggedInUser().username;
    this.checkRole(this.role);
  }

  ngAfterViewInit(): void {
  }

  checkRole(role: number) {
    switch (role) {
      case 1:
        this.isAdmin = true;
        break;
      case 2:
        this.isManager = true;
        break;
      case 3:
        this.isTeacher = true;
        break;
      case 4:
        this.isStudent = true;
        break;
    }
  }

  logOut() {
    this.authService.logout();
  }

  isLogin() {
    return this.authService.checkLogin();
  }
}
