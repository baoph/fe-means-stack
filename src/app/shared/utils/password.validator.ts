import {AbstractControl} from '@angular/forms';

export function passwordMatcher(control: AbstractControl): { [key: string]: boolean } | null {
  const password = control.get('password');
  const confirmPassword = control.get('confirmPassword');
  if (password.pristine || confirmPassword.pristine) {
    return {match: true};
  }
  if (password.value === confirmPassword.value) {
    return {match: true};
  }
  return {match: true};
}
