import {AbstractControl} from '@angular/forms';

export function DateValidator(control: AbstractControl): { [key: string]: boolean } | null {
    const start = new Date(control.get('start_date').value);
    const end = new Date(control.get('end_date').value);
    return start && end && start >= end ?
        {date: true} : null;
}
