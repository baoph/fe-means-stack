export interface ILookupArrayItem {
  key: string;
  value: string;
}

export type TLookupArray = ILookupArrayItem[];

export interface ILookup {
  [key: string]: {
    label: string;
  };
}

export class LookupUtils {
  public static MESSAGES = {
    HTTP_REQUEST_INVALID_DATA: 'Processing failed because the data size is too large.\n' +
      'It may be improved by reducing the number of input data lines or changing option settings.',
    BP_SEND_ERROR_DIALOG_BODY: 'A transmission error has occurred. Please retry the operation.',
    INSERT_SUCCESSFUL: 'You have successfully inserted',
    UPDATE_SUCCESSFUL: 'You have successfully updated',
    INVALID_VALIDATE: 'You have entered wrong information, please check the form again'
  };

  public static httpErrorCodeToMsg(code: number): string {
    if (code === 0) {
      // handle server is offline error
      return 'Unknown Error';
    } else if (code === 413) {
      return this.MESSAGES.HTTP_REQUEST_INVALID_DATA;
    } else {
      return `${this.MESSAGES.BP_SEND_ERROR_DIALOG_BODY}\n"${code}"`;
    }
  }

  // public static lookupKeyToLabel(lookup: ILookup, itemKey: string): string {
  //   return lookup[itemKey]['label'];
  // }
}
