import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {RegisterComponent} from '../../features/_core/auth/components/register/register.component';


@Injectable({
  providedIn: 'root'
})
export class RegisterGuard implements  CanDeactivate<RegisterComponent>{
  canDeactivate(component: RegisterComponent,
                currentRoute: ActivatedRouteSnapshot,
                currentState: RouterStateSnapshot,
                nextState?: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    if (component.loginForm.dirty) {
      return confirm(`Navigate away and lose all changes?`);
    }
    return true;
  }
}
