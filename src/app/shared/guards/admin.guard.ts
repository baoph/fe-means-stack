import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanLoad, Route, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '../services/http/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanLoad {
    constructor(private authService: AuthService, private router: Router) {
    }

    canLoad(route: Route): boolean {
        const url: string = route.path;
        if (this.authService.checkLogin()) {
            return true;
        }
        // Navigate to the login page with extras
        this.router.navigate(['/login'], { queryParams: { returnUrl: url } });
        return false;
    }
}
