import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from '../services/http/auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanLoad, CanActivate {
    constructor(private authService: AuthService, private router: Router) {
    }

    canLoad(route: Route): boolean {
        const url: string = route.path;
        if (this.authService.checkLogin()) {
            return true;
        }
        this.router.navigate(['/login'], {queryParams: {returnUrl: url}});
        return false;
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const url: string = state.url;
        if (this.authService.checkLogin()) {
            return true;
        }
        this.router.navigate(['/login'], {queryParams: {returnUrl: url}});
        return false;
    }
}
