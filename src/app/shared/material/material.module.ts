import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule, MatIconModule,
  MatInputModule, MatPaginatorModule,
  MatSelectModule, MatSortModule,
  MatTableModule, MatTooltipModule, MatDialogModule, MatDatepickerModule, MatNativeDateModule
} from '@angular/material';


const materialModule = [
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
  MatSelectModule,
  MatCardModule,
  MatIconModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatTooltipModule,
  MatDialogModule,
  MatDatepickerModule,
  MatNativeDateModule,
];

@NgModule({
  imports: [
    materialModule
  ],
  exports: [
    materialModule
  ]
})
export class MaterialModule {
}
