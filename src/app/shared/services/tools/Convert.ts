import {ElementRef} from '@angular/core';

export class Utilities {
    // tslint:disable-next-line:variable-name
    static list_extensionImage = ['image/bmp', 'image/png', 'image/jpeg'];

    static stringFilterNewline(content: string) {
        if (content.length === 0 || content.trim().length === 0) {
            return content;
        }
        return content.replace(/\n+/g, '</br>').trim();
    }

    static stringFilterBreakLine(content: string) {
        if (content.length === 0 || content.trim().length === 0) {
            return content;
        }
        return content.replace(/(<\/br>|<br>)+/g, '\n').trim();
    }

    // tslint:disable-next-line:variable-name
    static stringSafe(string_to_guard: string) {
        // tslint:disable-next-line:variable-name
        let string_safe_final = string_to_guard.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '-');
        string_safe_final = string_safe_final.split(' ').join('-');
        // tslint:disable-next-line:max-line-length
        string_safe_final = string_safe_final.charAt(string_safe_final.length - 1) === '-' ? string_safe_final.slice(0, string_safe_final.length - 1) : string_safe_final;
        string_safe_final = string_safe_final.charAt(0) === '-' ? string_safe_final.slice(1) : string_safe_final;
        return string_safe_final.toLowerCase().trim();
    }

    static render_static_image(url: string, element: ElementRef<HTMLCanvasElement>) {
        if (url == null || url.length < 2) {
            return;
        }
        const ctx: CanvasRenderingContext2D = element.nativeElement.getContext('2d');
        const image = new Image();
        image.onload = () => {
            ctx.canvas.width = image.width;
            ctx.canvas.height = image.height;
            ctx.drawImage(image, 0, 0);
        };
        image.src = url;
    }

    static checkingExtensionImages(file: File) {
        return Utilities.list_extensionImage.indexOf(file.type) >= 0;
    }

    static checkingSizeImages(file) {
        return file.size <= 7000000;
    }
}
