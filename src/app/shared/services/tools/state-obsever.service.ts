import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class StateObseverService {
    public scrollObserve: Subject<any> = new Subject();
    private messageSource = new BehaviorSubject(0);
    private courseSource = new BehaviorSubject(0);
    private roleSource = new BehaviorSubject(0);
    currentMessage = this.messageSource.asObservable();
    currentCourse = this.courseSource.asObservable();

    constructor() {
    }

    onScroll(event: any) {
        this.scrollObserve.next(event);
    }

    changeMessage(message: number) {
        this.messageSource.next(message);
    }
    changeCourse(course: number) {
        this.courseSource.next(course);
    }
}
