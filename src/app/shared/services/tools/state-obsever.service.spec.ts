import { TestBed } from '@angular/core/testing';

import { StateObseverService } from './state-obsever.service';

describe('StateObseverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StateObseverService = TestBed.get(StateObseverService);
    expect(service).toBeTruthy();
  });
});
