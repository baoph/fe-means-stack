// script.store.ts

interface Scripts {
    name: string;
    src: string;
}

export const ScriptStore: Scripts[] = [
    {name: 'datatable', src: 'assets/assets/examples/js/tables/datatable.js'},
    {name: 'editor', src: 'assets/assets/examples/js/forms/editor-summernote.js'},
    {name: 'droptify', src: 'assets/assets/examples/js/forms/uploads.js'}
];
