import { Injectable } from '@angular/core';

import { Socket } from 'ngx-socket-io';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Message} from '../../models/message';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private createMessageUrl = '/message/createMessage';
  private getMessagesByUserIdUrl = '/message/getMessageByUserId';
  private getMessagesByChatIdUrl = '/message/getMessagesByChatId';
  private getMessagesByRestaurantIdUrl = '/message/getMessageByRestaurantId';

  constructor(private socket: Socket,
              private http: HttpClient) { }

  getMessagesByChatId(id: string): Observable<Response> {
    const chatId = {id};
    return this.http.post<Response>(this.getMessagesByChatIdUrl, chatId).pipe(
      tap((res: any) => {
        const allMessages = [...res.data];
        this.socket.emit('getMessageByChatId', allMessages);
      })
    );
  }

  newMessage(message: Message): Observable<Response> {
    return this.http.post<Response>(this.createMessageUrl, message).pipe(
      tap((res: any) => {
        this.socket.emit('addMessage', res.data);
      })
    );
  }
}
