import {Injectable} from '@angular/core';
import {getUrl} from '../../commons/constrants';
import {HttpClient} from '@angular/common/http';
import {GeneralService} from './general.service';
import {Observable} from 'rxjs';

import {tap} from 'rxjs/operators';
import {Schedule} from '../../models/schedule';

@Injectable({
    providedIn: 'root'
})
export class ScheduleService {
    scheduleUrl = getUrl() + 'api/subjects';
    createScheduleUrl = getUrl() + 'api/createSchedule/';
    getCourseScheduleUrl = getUrl() + 'api/getCourseSchedule/';
    getStudentScheduleUrl = getUrl() + 'api/getStudentSchedule/';
    getTeacherScheduleUrl = getUrl() + 'api/getTeacherSchedule/';

    constructor(
        private http: HttpClient,
        private generalService: GeneralService
    ) {
    }

    getSubjects(): Observable<Schedule[]> {
        return this.generalService.getList<Schedule>(this.scheduleUrl);
    }

    getCourseSchedule(id: string): Observable<Schedule[]> {
        const url = this.getCourseScheduleUrl + id;
        return this.http.get<Schedule[]>(url)
            .pipe(
                tap(data => console.log(data))
            );
    }

    getStudentSchedule(id: string): Observable<Schedule[]> {
        const url = this.getStudentScheduleUrl + id;
        console.log(url);
        return this.http.get<Schedule[]>(url)
            .pipe(
                tap(data => console.log(data))
            );
    }

    getTeacherSchedule(id: string): Observable<Schedule[]> {
        const url = this.getTeacherScheduleUrl + id;
        return this.http.get<Schedule[]>(url)
            .pipe(
                tap(data => console.log(data))
            );
    }

    getSchedule(id: number): Observable<Schedule> {
        return this.generalService.getByID<Schedule>(id, this.scheduleUrl);
    }

    editSchedule(c: Schedule): Observable<Schedule> {
        return this.generalService.edit<Schedule>(c, this.scheduleUrl);
    }

    createSchedule(c: Schedule): Observable<Schedule> {
        return this.generalService.create<Schedule>(c, this.createScheduleUrl);
    }

    deleteSchedule(id: number): Observable<Schedule> {
        return this.generalService.delete<Schedule>(id, this.scheduleUrl);
    }
}
