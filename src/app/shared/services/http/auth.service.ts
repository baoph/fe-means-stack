import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';
import {LoggedInUser, User} from '../../models/user';

const JwtHelper = new JwtHelperService();


@Injectable()
export class AuthService {
  APIloginUrl = '/users/login';
  // APIloginUrl = 'http://phplaravel-256943-893077.cloudwaysapps.com/api/auth/login';
  loggedInUser: LoggedInUser;

  constructor(private http: HttpClient,
              private router: Router) {
  }

  static getToken(): string | null {
    return localStorage.getItem('access_token');
  }

  getRole(): string | null {
    this.loggedInUser = JSON.parse(localStorage.getItem('user'));
    const role = this.loggedInUser.role;
    return role;
  }

  getUserId(): string | null {
    this.loggedInUser = JSON.parse(localStorage.getItem('user'));
    const id = this.loggedInUser._id;
    return id;
  }

  getEmail(): string | null {
    this.loggedInUser = JSON.parse(localStorage.getItem('user'));
    const email = this.loggedInUser.email;
    return email;
  }

  getLoggedInUser(): LoggedInUser {
    return JSON.parse(localStorage.getItem('user'));
  }

  getRestaurantId(): string {
    const user = JSON.parse(localStorage.getItem('user'));
    const id = user.hasOwnProperty('restaurant') ? user.restaurant : '';
    return id;
  }

  login(user: User) {
    return this.http.post<LoggedInUser>(this.APIloginUrl, user);
  }

  logout() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('user');
    this.loggedInUser = null;
    this.router.navigate(['login']);
  }


  checkLogin(): boolean {
    const currentUser = JSON.parse(localStorage.getItem('user'));
    if (currentUser) {
      return true;
    }
    return false;
  }

  public isLoggedIn() {
    const token = localStorage.getItem('access_token');
    return token ? JwtHelper.isTokenExpired(token) : false;
  }

  setSession(authResult) {
    // const expiresAt = moment().add(authResult.expires_in, 'second');
    // localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
    localStorage.setItem('access_token', authResult.data.token);
    localStorage.setItem('user', JSON.stringify(authResult.data.user));
  }
}
