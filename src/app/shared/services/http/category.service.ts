import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {getUrl} from '../../commons/constrants';
import {Category} from '../../models/Category';
import {GeneralService} from './general.service';
import {catchError, tap} from 'rxjs/operators';
import {ApiResponse} from '../../models/response';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  categoryUrl = '/category/getAllCategory';

  constructor(
    private http: HttpClient,
    private generalService: GeneralService
  ) {
  }

  /**
   * Get list of article
   */
  getAllCategories(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.categoryUrl);
  }

  private initializeCategory(): Category {
    return {
      _id: null,
      categoryName: null
    };
  }
  private handleError(err) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }
}
