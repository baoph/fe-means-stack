import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';

@Injectable()
export class HttpResponseHandler {
  constructor(
    private router: Router,
  ) {
  }

  /**
   * Global http error handler.
   *
   * param response
   * param source
   * returns {ErrorObservable}
   */
  public onCatch(response: any, source: Observable<any>): Observable<any> {
    switch (response.status) {
      case 400:
        this.handleBadRequest(response);
        break;

      case 401:
        this.handleUnauthorized(response);
        break;

      case 403:
        this.handleForbidden();
        break;

      case 404:
        this.handleNotFound(response);
        break;

      case 500:
        this.handleServerError();
        break;

      default:
        break;
    }

    return throwError(response);
  }

  /**
   * Shows notification errors when server response status is 401
   *
   * param responseBody
   */
  private handleBadRequest(responseBody: any): void {
    if (responseBody._body) {
      try {
        const bodyParsed = responseBody.json();
        this.handleErrorMessages(bodyParsed);
      } catch (error) {
        this.handleServerError();
      }
    } else {
      this.handleServerError();
    }
  }

  /**
   * Shows notification errors when server response status is 401 and redirects user to login page
   *
   * param responseBody
   */
  private handleUnauthorized(responseBody: any): void {
    //    logic handle
  }

  /**
   * Shows notification errors when server response status is 403
   */
  private handleForbidden(): void {
    //    logic handle
  }

  /**
   * Shows notification errors when server response status is 404
   *
   * param responseBody
   */
  private handleNotFound(responseBody: any): void {
    //    logic handle
  }

  /**
   * Shows notification errors when server response status is 500
   */
  private handleServerError(): void {
    //    logic handle
  }

  /**
   * Parses server response and shows notification errors with translated messages
   *
   * param response
   */
  private handleErrorMessages(response: any): void {
    if (!response) {
      return;
    }

    for (const key of Object.keys(response)) {
      if (Array.isArray(response[key])) {
        response[key].forEach(value =>
          this.showNotificationError('Error', this.getTranslatedValue(value))
        );
      } else {
        this.showNotificationError('Error', this.getTranslatedValue(response[key]));
      }
    }
  }

  /**
   * Extracts and returns translated value from server response
   *
   * param value
   * returns {string}
   */
  private getTranslatedValue(value: string): string {
    //    logic handle
    return value;
  }

  /**
   * Returns relative url from the absolute path
   *
   * returns {string}
   * param url
   */
  private getRelativeUrl(url: string): string {
    return url.toLowerCase().replace(/^(?:\/\/|[^\/]+)*\//, '');
  }

  /**
   * Shows error notification with given title and message
   *
   * param title
   * param message
   */
  private showNotificationError(title: string, message: string): void {
    //    logic handle
  }
}

