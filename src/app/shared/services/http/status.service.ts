import { Injectable } from '@angular/core';
import { GeneralService } from './general.service';
import { ActiveStatus, PublishStatus } from '../../models/status';
import { Observable } from 'rxjs';
import { getUrl } from '../../commons/constrants';

@Injectable({
    providedIn: 'root'
})
export class StatusService {
    activeUrl = getUrl() + 'api/getActiveStatus';
    publishUrl = getUrl() + 'api/getPublishStatus';
    constructor(private generalService: GeneralService) { }


    getAtiveStatus(): Observable<ActiveStatus[]> {
        return this.generalService.getList<ActiveStatus>(this.activeUrl);
    }

    getPublishStatus(): Observable<PublishStatus[]> {
        return this.generalService.getList<PublishStatus>(this.publishUrl);
    }
}
