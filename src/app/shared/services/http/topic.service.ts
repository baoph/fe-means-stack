import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {getUrl} from '../../commons/constrants';
import {Topic} from '../../models/Topic';
import {GeneralService} from './general.service';
import {catchError, tap} from 'rxjs/operators';

@Injectable()
export class TopicService {
  topicUrl = getUrl() + 'api/topics';

  constructor(
    private http: HttpClient,
    private generalService: GeneralService
  ) {
  }

  /**
   * Get list of article
   */
  getTopics(): Observable<Topic[]> {
    return this.generalService.getList<Topic>(this.topicUrl);
  }

  /**
   * get a article by id
   * param id
   */
  getTopic(id: number): Observable<Topic> {
      const url = `${this.topicUrl}/${id}`;
      return this.http.get<Topic>(url)
          .pipe(
              tap(data => console.log(data)),
              catchError(this.handleError)
          );
  }

  // edit an article
  editTopic(topic: Topic): Observable<Topic> {
    return this.generalService.edit<Topic>(topic, this.topicUrl);
  }

  // create an new article
  createTopic(topic: Topic): Observable<Topic> {
    return this.generalService.create<Topic>(topic, this.topicUrl);
  }

  // delete an article by id
  deleteTopic(id: number): Observable<Topic> {
    return this.generalService.delete<Topic>(id, this.topicUrl);
  }

  private initializeTopic(): Topic {
    return {
      id: 0,
      topicName: null
    };
  }
    private handleError(err) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        let errorMessage: string;
        if (err.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
        }
        console.error(err);
        return throwError(errorMessage);
    }
}
