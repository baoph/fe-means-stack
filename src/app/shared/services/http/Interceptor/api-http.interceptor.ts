import { throwError, Observable, of } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import {IApiError} from '../../../models/IApiError';
import {LookupUtils} from '../../../utils/lookup';
import {environment} from '../../../../../environments/environment';


@Injectable()
export class ApiHttpInterceptor implements HttpInterceptor {
  private readonly DEV_PROXY: string;

  constructor() {
    this.DEV_PROXY = environment.devProxy;
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    if (environment.debug) {
      return this.interceptDebug(req, next);
    } else {
      return next.handle(req).pipe(
        catchError((errorResponse) => {
          console.warn('Interceptor: Error Response', errorResponse);
          let errorMsg = '';
          if (errorResponse instanceof HttpErrorResponse) {
            if (errorResponse.error instanceof ProgressEvent) {
              // handle server doesnt respond error
              errorMsg = 'Failed to receive response';
            } else if (errorResponse.error) {
              // handle backend error
              if (Array.isArray(errorResponse.error.errors)) {
                const errors: IApiError[] = [...errorResponse.error.errors];
                errors.forEach((err) => {
                  console.warn(`Interceptor: API error`, err);
                  errorMsg += err.message;
                });
              } else {
                errorMsg = LookupUtils.httpErrorCodeToMsg(errorResponse.status);
              }
            } else {
              return throwError('There was a network error');
            }
          } else {
            // handle other errors, exception gets thrown in an RxJS operator
            errorMsg = `RxJS error:${errorResponse}`;
          }
          return throwError(errorMsg);
        }),
      );
    }
  }

  interceptDebug(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    if (!req.url.includes('assets/css/')) {
      req = req.clone({
        url: `${this.DEV_PROXY}${req.url}`,
        withCredentials: true,
      });
    }
    return next.handle(req);
  }
}
