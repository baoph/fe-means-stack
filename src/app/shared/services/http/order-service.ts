import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Order} from '../../models/order';
import {map, tap} from 'rxjs/operators';
import {Article} from '../../models/article';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private createOrderUrl = '/order/createOrder';
  private getAllOrdersUrl = '/order/getAllOrders';
  private searchOrderUrl = '/order/searchOrder';
  private getOrderUrl = '/order/getOrder';
  private updateOrdereUrl = '/order/updateOrder';

  constructor(private http: HttpClient) {
  }
  createOrder(order: Order): Observable<Response> {
    return this.http.post<Response>(this.createOrderUrl, order);
  }

  getAllOrders(): Observable<Order[]> {
    return this.http.get<Order[]>(this.getAllOrdersUrl);
  }

  searchOrder(text: string): Observable<Response> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post<Response>(this.searchOrderUrl, text, {headers});
  }

  getOrder(id: string): Observable<Order> {
    const orderId = {id};
    return this.http.post<Order>(this.getOrderUrl, orderId);
  }

  updateOrder(res: Order): Observable<Order> {
    return this.http.put<Article>(this.updateOrdereUrl, res)
      .pipe(
        tap(() => console.log('Updated Article: ' + res._id)),
        map(() => res)
      );
  }
}
