import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {getUrl} from '../../commons/constrants';

import {GeneralService} from './general.service';

import {catchError, tap} from 'rxjs/operators';
import {Cmt} from '../../models/cmt';

@Injectable({
        providedIn: 'root'
    }
)
export class CommentService {
    commentUrl = getUrl() + 'api/comments';

    constructor(
        private http: HttpClient,
        private generalService: GeneralService
    ) {
    }

    getComments(): Observable<Cmt[]> {
        return this.generalService.getList<Cmt>(this.commentUrl);
    }


    createComment(comment: Cmt): Observable<Cmt> {
        return this.http.post<Cmt>(this.commentUrl, comment)
            .pipe(
                tap(data => console.log('Create an new article: ' + JSON.stringify(data)))
            );
    }
}
