import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {getUrl} from '../../commons/constrants';
import {Room} from '../../models/Room';
import {GeneralService} from './general.service';
import {Article} from '../../models/article';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
        providedIn: 'root'
    }
)
export class RoomService {
    RoomUrl = getUrl() + 'api/rooms';

    constructor(
        private http: HttpClient,
        private generalService: GeneralService
    ) {
    }

    /**
     * Get list of article
     */
    getRooms(): Observable<Room[]> {
        return this.generalService.getList<Room>(this.RoomUrl);
    }

    /**
     * get a article by id
     * param id
     */
    getRoom(id: number): Observable<Room> {
        return this.generalService.getByID<Room>(id, this.RoomUrl);
    }

    // edit an article
    editRoom(room: Room): Observable<Room> {
        return this.generalService.edit<Room>(room, this.RoomUrl);
    }

    // create an new article

    createRoom(room: Room): Observable<Room> {
        return this.http.post<Room>(this.RoomUrl, room)
            .pipe(
                tap(data => console.log('Create an new room: ' + JSON.stringify(data)))
            );
    }

    // delete an article by id
    deleteRoom(id: number): Observable<Room> {
        return this.generalService.delete<Room>(id, this.RoomUrl);
    }
}
