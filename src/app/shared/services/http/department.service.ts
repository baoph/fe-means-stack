import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {getUrl} from '../../commons/constrants';
import {Department} from '../../models/department';
import {GeneralService} from './general.service';
import {Article} from '../../models/article';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
    }
)
export class DepartmentService {
    departmentUrl = getUrl() + 'api/departments';

    constructor(
        private http: HttpClient,
        private generalService: GeneralService
    ) {
    }

    /**
     * Get list of article
     */
    getDepartments(): Observable<Department[]> {
        return this.generalService.getList<Department>(this.departmentUrl);
    }

    /**
     * get a article by id
     * param id
     */
    getDepartment(id: number): Observable<Department> {
        if (id === 0) {
            return of(this.initializeDepartment());
        }
        return this.generalService.getByID<Department>(id, this.departmentUrl);
    }

    // edit an article
    editDepartment(department: Department): Observable<Department> {
        return this.generalService.edit<Department>(department, this.departmentUrl);
    }

    // create an new article

    createDepartment(department: Department): Observable<Department> {
        return this.http.post<Department>(this.departmentUrl, department)
            .pipe(
                tap(data => console.log('Create an new article: ' + JSON.stringify(data)))
            );
    }

    // delete an article by id
    deleteDepartment(id: number): Observable<Department> {
        return this.generalService.delete<Department>(id, this.departmentUrl);
    }

    private initializeDepartment(): Department {
        return {
            id: 0,
            departmentName: null,
            created_at: null,
            updated_at: null
        };
    }
}
