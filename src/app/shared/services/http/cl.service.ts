import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {getUrl} from '../../commons/constrants';
import {Class} from '../../models/Class';
import {GeneralService} from './general.service';
import {tap} from 'rxjs/operators';

@Injectable({
        providedIn: 'root'
    }
)
export class ClService {
    classUrl = getUrl() + 'api/courses';
    getClassByDepartmentUrl = getUrl() + 'api/getCourseByDepartment/';

    constructor(
        private http: HttpClient,
        private generalService: GeneralService
    ) {
    }

    /**
     * Get list of article
     */
    getClasses(): Observable<Class[]> {
        return this.generalService.getList<Class>(this.classUrl);
    }

    getClassByDepartment(id: string): Observable<Class[]> {
        const url = this.getClassByDepartmentUrl + id;
        return this.http.get<Class[]>(url)
            .pipe(
                tap(data => console.log(data))
            );
    }

    /**
     * get a article by id
     * param id
     */
    getClass(id: number): Observable<Class> {
        return this.generalService.getByID<Class>(id, this.classUrl);
    }

    // edit an article
    editClass(c: Class): Observable<Class> {
        return this.generalService.edit<Class>(c, this.classUrl);
    }

    // create an new article

    createClass(c: Class): Observable<Class> {
        return this.generalService.create<Class>(c, this.classUrl);
    }

    // delete an article by id
    deleteClass(id: number): Observable<Class> {
        return this.generalService.delete<Class>(id, this.classUrl);
    }
}
