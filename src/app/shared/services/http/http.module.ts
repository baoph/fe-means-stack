import {ModuleWithProviders, NgModule} from '@angular/core';
import {ArticleService} from './article.service';
import {AuthService} from './auth.service';
import {HttpResponseHandler} from './http.response-handler.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {Script} from './script.service';
import {GeneralService} from './general.service';
import {TopicService} from './topic.service';
import {StatusService} from './status.service';
import {HttpResponseInterceptor} from './Interceptor/http.interceptor';
import {HttpErrorInterceptor} from './Interceptor/http-error.interceptor';
import {RestaurantService} from './restaurant.service';
import {ApiHttpInterceptor} from './Interceptor/api-http.interceptor';
import {CategoryService} from './category.service';
import {PromotionService} from './promotion.service';
import {OrderService} from './order-service';
import {DocumentService} from './document.service';
import {MessageService} from './message.service';

@NgModule({
  imports: [CommonModule, HttpClientModule]
})
export class HttpServiceModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: HttpServiceModule,
      providers: [
        ArticleService,
        AuthService,
        TopicService,
        CategoryService,
        StatusService,
        RestaurantService,
        Script,
        GeneralService,
        HttpResponseHandler,
        PromotionService,
        OrderService,
        DocumentService,
        MessageService,
        {provide: HTTP_INTERCEPTORS, useClass: HttpResponseInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ApiHttpInterceptor, multi: true}
      ]
    };
  }
}
