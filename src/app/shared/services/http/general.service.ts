import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Article } from '../../models/article';

@Injectable()
export class GeneralService {
    constructor(private http: HttpClient) {
    }

    /**
     *
     * param url
     */
    getList<Type>(url): Observable<Type[]> {
        return this.http.get<Type[]>(url)
            .pipe(
                tap(data => console.log(data))
            );
    }

    // get a article by id
    getByID<Type>(id: number, apiUrl: string): Observable<Type> {
        // if (id === 0) {
        //   return of(this.initializeItem());
        // }
        const url = `${apiUrl}/${id}`;
        return this.http.get<Type>(url)
            .pipe(
                tap(data => console.log(JSON.stringify(data)))
            );
    }

    // edit an article
    edit<Type>(type: Type, apiUrl: string): Observable<Type> {
        return this.http.put<Type>(apiUrl, type)
            .pipe(
                tap(() => console.log('Updated Article: ' + type)),
                map(() => type)
            );
    }

    // create an new article
    create<Type>(type: Type, apiUrl: string): Observable<Type> {
        return this.http.post<Type>(apiUrl, type)
            .pipe(
                tap(data => console.log('Create an new article: ' + JSON.stringify(data)))
            );
    }

    // delete an article by id
    delete<Type>(id: number, apiUrl: string): Observable<Type> {
        const deleteURL = `${apiUrl}/${id}`;
        return this.http.delete<Type>(deleteURL)
            .pipe(
                tap(data => console.log('deleteProduct: ' + id))
            );
    }

    private initializeItem<Type>(): Type {
        return null;
    }
}
