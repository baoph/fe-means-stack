import {Injectable} from '@angular/core';

import {Socket} from 'ngx-socket-io';

import {Document} from '../../models/document';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Chat} from '../../models/chat';
import {tap} from 'rxjs/operators';
import {Message} from '../../models/message';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {
  currentDocument = this.socket.fromEvent<Document>('document');
  documents = this.socket.fromEvent<string[]>('documents');
  messages = this.socket.fromEvent<Message[]>('messages');
  private createChatUrl = '/chat/createChat';
  private getChatIdUrl = '/chat/getChatId';
  private getChatsByUserIdUrl = '/chat/getChatByUserId';
  private getChatsByRestaurantIdUrl = '/chat/getChatByRestaurantId';

  constructor(private socket: Socket,
              private http: HttpClient) {
  }

  getDocument(id: string) {
    this.socket.emit('getDoc', id);
  }

  getChatsByUserId(id: string): Observable<Response> {
    const userId = {id};
    return this.http.post<Response>(this.getChatsByUserIdUrl, userId).pipe(
      tap((res: any) => {
        const allChats = [];
        res.data.forEach(chat => {
          allChats.push(chat._id);
        });
        this.socket.emit('getDocsByUserId', allChats);
      })
    );
  }

  getChatId(userId, restaurantId): Observable<Response> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    const params = {
      user: userId,
      restaurant: restaurantId
    };
    return this.http.post<Response>(this.getChatIdUrl, params, {headers});
  }

  getChatsByRestaurantId(id: string): Observable<Response> {
    const restaurantId = {id};
    return this.http.post<Response>(this.getChatsByRestaurantIdUrl, restaurantId).pipe(
      tap((res: any) => {
        const allChats = {};
        res.data.forEach(chat => {
          allChats[chat._id] = '';
        });
        this.socket.emit('getDocsByRestaurantId', allChats);
      })
    );
  }

  newDocument(chat: Chat): Observable<Response> {
    return this.http.post<Response>(this.createChatUrl, chat).pipe(
      tap((res: any) => {
        console.log(res.data._id);
        this.socket.emit('addDoc', {_id: res.data._id, doc: ''});
      })
    );
  }

  editDocument(document: Document) {
    this.socket.emit('editDoc', document);
  }

  private docId() {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }
}
