import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, map, retry, tap} from 'rxjs/operators';
import {Article} from '../../models/article';
import {getUrl} from '../../commons/constrants';
import {AuthService} from './auth.service';
import {ActionResponse, ArticleCollectionResponse} from '../../models/ResponseModel';

@Injectable()
export class ArticleService {
  private getArticleUrl = '/article/getArticle';
  private createNewArticleUrl = '/article/upload/createNewArticle';
  private updateArticleUrl = '/article/updateArticle';

  constructor(private http: HttpClient,
              private authService: AuthService) {
  }

// get a article by restaurant id
  getArticle(id: string): Observable<Article> {
    console.log(id);
    if (!id) {
      return of(this.initializeArticle());
    }
    return this.http.post<Article>(this.getArticleUrl, id);
  }

  articlePaginate(urlPaginate: string): Observable<ArticleCollectionResponse> {
    const errModel: ArticleCollectionResponse = {
      data: [],
      links: null,
      meta: null
    };

    return this.http.get<ArticleCollectionResponse>(urlPaginate)
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        retry(3),
        tap(res => res),
        catchError(err => {
          console.log(err);
          return of(errModel);
        })
      );
  }

  // edit an article
  editArticle(article: Article): Observable<Article> {
    return this.http.post<Article>(this.updateArticleUrl, article)
      .pipe(
        tap(() => console.log('Updated Article: ' + article._id)),
        map(() => article)
      );
  }

  // create an new article
  createArticle(article: Article): Observable<Article> {
    return this.http.post<Article>(this.createNewArticleUrl, article)
      .pipe(
        tap(data => console.log('Create an new article: ' + JSON.stringify(data)))
      );
  }

  // delete an article by id
  // deleteArticle(id: number): Observable<Article> {
  //   const deleteURL = `${this.articleUrl}/${id}`;
  //   return this.http.delete<Article>(deleteURL)
  //     .pipe(
  //       tap(data => console.log('deleteProduct: ' + id))
  //     );
  // }

  changeStatusArticle(article: Article) {
    const changeUrl = getUrl() + 'api/changeStatusArticle';
    return this.http.put<ActionResponse>(changeUrl, article)
      .pipe(
        tap(data => console.log('code: ' + data.code))
      );
  }

  // upload article
  uploadArticle(formData: {
    _id: string,
    articleContent: string,
    articleImage: string,
    images: File,
    priceRange: string,
    category: string,
  }): Observable<ActionResponse> {
    const err: ActionResponse = {
      error: true,
      code: null,
      message: 'Unable to connect to server',
    };
    // const token = AuthService.getToken();
    // if (token === null) {
    //     return of(err);
    // }
    const fd = new FormData();
    fd.append('_id', formData._id);
    fd.append('articleContent', formData.articleContent);
    fd.append('articleImage', formData.articleImage);
    fd.append('priceRange', formData.priceRange);
    fd.append('category', formData.category);
    fd.append('files', formData.images);
    return this.http.post<ActionResponse>(this.updateArticleUrl, fd)
      .pipe(
        tap(res => {
          return res;
        })
      );
  }

  private initializeArticle(): Article {
    return {
      _id: null,
      articleContent: null,
      articleImage: null,
      category: null,
      priceRange: null
    };
  }

}
