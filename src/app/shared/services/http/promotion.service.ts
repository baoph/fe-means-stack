import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Promotion} from '../../models/Promotion';
import {ApiResponse} from '../../models/response';

@Injectable()
export class PromotionService {
  private createPromotionUrl = '/promotion/createPromotion';
  private updatePromotionUrl = '/promotion/updatePromotion';
  private getPromotionUrl = '/promotion/getPromotion';
  private getPromotionByRestaurantUrl = '/promotion/getPromotionByRestaurant';
  private deletePromotionUrl = '/promotion/deletePromotion';

  constructor(
    private http: HttpClient,
  ) {
  }

  /**
   * Get list of article
   */
  getPromotionByRestaurant(restaurantId: string): Observable<ApiResponse> {
    const param = {restaurantId};
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post<ApiResponse>(this.getPromotionByRestaurantUrl,
      JSON.stringify(param), {headers});
  }

  /**
   * get a article by id
   * param id
   */
  getPromotion(id: string): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.getPromotionUrl, id);
  }

  // edit an promotion
  editPromotion(promotion: Promotion): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.updatePromotionUrl, promotion);
  }

  // create an new promotion
  createPromotion(promotion: Promotion): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.createPromotionUrl, promotion);
  }

  // delete an promotion by id
  deletePromotion(id: string): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.deletePromotionUrl, id);
  }
}
