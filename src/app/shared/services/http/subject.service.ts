import { Injectable } from '@angular/core';
import {getUrl} from '../../commons/constrants';
import {HttpClient} from '@angular/common/http';
import {GeneralService} from './general.service';
import {Observable} from 'rxjs';

import {tap} from 'rxjs/operators';
import {Subject} from '../../models/subject';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
    SubjectUrl = getUrl() + 'api/subjects';
    getSubjectByCourseUrl = getUrl() + 'api/getSubjectByCourse/';

    constructor(
        private http: HttpClient,
        private generalService: GeneralService
    ) {
    }

    getSubjects(): Observable<Subject[]> {
        return this.generalService.getList<Subject>(this.SubjectUrl);
    }

    getSubjectByCourse(id: string): Observable<Subject[]> {
        const url = this.getSubjectByCourseUrl + id;
        return this.http.get<Subject[]>(url)
            .pipe(
                tap(data => console.log(data))
            );
    }

    getSubject(id: number): Observable<Subject> {
        return this.generalService.getByID<Subject>(id, this.SubjectUrl);
    }

    editSubject(c: Subject): Observable<Subject> {
        return this.generalService.edit<Subject>(c, this.SubjectUrl);
    }

    createSubject(c: Subject): Observable<Subject> {
        return this.generalService.create<Subject>(c, this.SubjectUrl);
    }

    deleteSubject(id: number): Observable<Subject> {
        return this.generalService.delete<Subject>(id, this.SubjectUrl);
    }
}
