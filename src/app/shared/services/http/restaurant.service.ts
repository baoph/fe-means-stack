import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Restaurant} from '../../models/restaurant';
import {map, tap} from 'rxjs/operators';
import {Article} from '../../models/article';

@Injectable()
export class RestaurantService {
  private createRestaurantUrl = '/restaurant/createRestaurant';
  private getAllRestaurantsUrl = '/restaurant/getAllRestaurants';
  private searchRestaurantUrl = '/restaurant/searchRestaurant';
  private getRestaurantUrl = '/restaurant/getRestaurant';
  private updateRestauranteUrl = '/restaurant/updateRestaurant';

  constructor(private http: HttpClient) {
  }

  getAllRestaurants(): Observable<Restaurant[]> {
    return this.http.get<Restaurant[]>(this.getAllRestaurantsUrl);
  }

  searchRestaurant(text: string): Observable<Response> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post<Response>(this.searchRestaurantUrl, text, {headers});
  }

  getRestaurant(id: string): Observable<Restaurant> {
    if (!id) {
      return of(this.initializeRestaurant());
    }
    const restaurantId = {id};
    return this.http.post<Restaurant>(this.getRestaurantUrl, restaurantId);
  }

  updateRestaurant(res: Restaurant): Observable<Restaurant> {
    return this.http.put<Article>(this.updateRestauranteUrl, res)
      .pipe(
        tap(() => console.log('Updated Article: ' + res._id)),
        map(() => res)
      );
  }

  private initializeRestaurant(): Restaurant {
    return {
      _id: null,
      username: null,
      tel: null,
      email: null,
      resName: null,
      resAddress: null,
      article: null
    };
  }
}
