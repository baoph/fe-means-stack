import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {getUrl} from '../../commons/constrants';
import {Role} from '../../models/Role';
import {GeneralService} from './general.service';

@Injectable()
export class RoleService {
  RoleUrl = getUrl() + 'api/Roles';

  constructor(
    private http: HttpClient,
    private generalService: GeneralService
  ) {
  }

  /**
   * Get list of article
   */
  getRoles(): Observable<Role[]> {
    return this.generalService.getList<Role>(this.RoleUrl);
  }

  /**
   * get a article by id
   * param id
   */
  getRole(id: number): Observable<Role> {
    if (id === 0) {
      return of(this.initializeRole());
    }
    return this.generalService.getByID<Role>(id, this.RoleUrl);
  }

  // edit an article
  editRole(role: Role): Observable<Role> {
    return this.generalService.edit<Role>(role, this.RoleUrl);
  }

  // create an new article
  createRole(role: Role): Observable<Role> {
    return this.generalService.create<Role>(role, this.RoleUrl);
  }

  // delete an article by id
  deleteRole(id: number): Observable<Role> {
    return this.generalService.delete<Role>(id, this.RoleUrl);
  }

  private initializeRole(): Role {
    return {
      id: 0,
      roleName: null
    };
  }
}
