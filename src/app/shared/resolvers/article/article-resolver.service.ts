// import {Injectable} from '@angular/core';
// import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
//
//
// import {Observable, of} from 'rxjs';
// import {catchError, map} from 'rxjs/operators';
// import {ArticleService} from '../../services/http/article.service';
// import {Article, ArticleResolved} from '../../models/article';
//
//
// @Injectable({
//     providedIn: 'root'
// })
// export class ArticleResolver implements Resolve<ArticleResolved> {
//
//     constructor(private articleService: ArticleService) {
//     }
//
//     resolve(route: ActivatedRouteSnapshot,
//             state: RouterStateSnapshot):
//         Observable<ArticleResolved> {
//         return this.articleService.getArticles()
//             .pipe(
//                 map(res => ({article: res})),
//                 catchError(err => {
//                     const message = `Retrieval error ${err}`;
//                     console.log(message);
//                     return of({article: null, error: message});
//                 })
//             );
//     }
// }
