import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ArticleEditResolved} from '../../models/article';
import {RestaurantService} from '../../services/http/restaurant.service';
import {AuthService} from '../../services/http/auth.service';


@Injectable({
  providedIn: 'root'
})
export class ArticleEditResolver implements Resolve<ArticleEditResolved> {

  constructor(private restaurantService: RestaurantService,
              private authService: AuthService) {
  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot):
    Observable<ArticleEditResolved> {
    const id = this.authService.getRestaurantId();
    if (!id) {
      const message = `Article not found: ${id}`;
      return of({article: null, error: message});
    }
    return this.restaurantService.getRestaurant(id)
      .pipe(
        map((articleOnEditing: any) => {
          console.log(articleOnEditing);
          if (articleOnEditing.code === 200) {
            return {article: articleOnEditing.data[0].article};
          }
          return {article: null, err: articleOnEditing.message};
        }),
        catchError(err => {
          const message = `Retrieval error ${err}`;
          return of({article: null, error: message});
        })
      );
  }
}
