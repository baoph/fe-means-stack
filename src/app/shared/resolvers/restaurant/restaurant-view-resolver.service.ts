import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';

import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {RestaurantService} from '../../services/http/restaurant.service';
import {RestaurantEditResolved} from '../../models/restaurant';
import {AuthService} from '../../services/http/auth.service';


@Injectable({
  providedIn: 'root'
})
export class RestaurantViewResolver implements Resolve<RestaurantEditResolved> {

  constructor(private resService: RestaurantService) {
  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot):
    Observable<RestaurantEditResolved> {
    const id = route.paramMap.get('id');
    if (!id) {
      const message = `Restaurant not found: ${id}`;
      return of({restaurant: null, error: message});
    }
    return this.resService.getRestaurant(id)
      .pipe(
        map((restaurantOnEditing: any) => {
          if (restaurantOnEditing.code === 200) {
            return {
              restaurant: restaurantOnEditing.data
            };
          }
          return {restaurant: null, err: restaurantOnEditing.message};
        }),
        catchError(err => {
          const message = `Retrieval error ${err}`;
          return of({restaurant: null, error: message});
        })
      );
  }
}
