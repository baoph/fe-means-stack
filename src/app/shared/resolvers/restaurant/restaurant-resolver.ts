import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';


import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {RestaurantService} from '../../services/http/restaurant.service';
import {RestaurantResolved} from '../../models/restaurant';


@Injectable({
  providedIn: 'root'
})
export class RestaurantResolver implements Resolve<RestaurantResolved> {

  constructor(private restaurantService: RestaurantService) {
  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot):
    Observable<RestaurantResolved> {
    return this.restaurantService.getAllRestaurants()
      .pipe(
        map((res: any) => ({restaurants: res})),
        catchError(err => {
          const message = `Retrieval error ${err}`;
          console.log(message);
          return of({restaurants: null, error: message});
        })
      );
  }
}
