import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';


import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ArticleResolved} from '../../models/article';
import {DepartmentService} from '../../services/http/department.service';
import {DepartmentResolved} from '../../models/department';


@Injectable({
    providedIn: 'root'
})
export class DepartmentResolver implements Resolve<DepartmentResolved> {

    constructor(private departmentService: DepartmentService) {
    }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot):
        Observable<DepartmentResolved> {
        return this.departmentService.getDepartments()
            .pipe(
                map(d => ({departments: d})),
                catchError(err => {
                    const message = `Retrieval error ${err}`;
                    console.log(message);
                    return of({departments: null, error: message});
                })
            );
    }
}
