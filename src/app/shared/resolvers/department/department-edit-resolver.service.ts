import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';

import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Department, DepartmentEditResolved} from '../../models/department';
import {DepartmentService} from '../../services/http/department.service';


@Injectable({
  providedIn: 'root'
})
export class DepartmentEditResolver implements Resolve<DepartmentEditResolved> {

  constructor(private departmentService: DepartmentService) {
  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot):
    Observable<DepartmentEditResolved> {
    const id = route.paramMap.get('id');
    if (isNaN(+id)) {
      const message = `Articles id was not a number: ${id}`;
      return of({department: null, error: message});
    }
    return this.departmentService.getDepartment(+id)
      .pipe(
        map(departmentOnEditing => ({department: departmentOnEditing})),
        catchError(err => {
          const message = `Retrieval error ${err}`;
          console.log(message);
          return of({department: null, error: message});
        })
      );
  }
}
