import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';


import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {PromotionService} from '../../services/http/promotion.service';
import {PromotionResolved} from '../../models/promotion';
import {AuthService} from '../../services/http/auth.service';


@Injectable({
  providedIn: 'root'
})
export class PromotionResolver implements Resolve<PromotionResolved> {

  constructor(private promotionService: PromotionService,
              private authService: AuthService) {
  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot):
    Observable<PromotionResolved> {
    const id = this.authService.getRestaurantId();
    if (!id) {
      const message = `Restaurant not found: ${id}`;
      return of({promotion: null, error: message});
    }
    return this.promotionService.getPromotionByRestaurant(id)
      .pipe(
        map((res: any) => {
            if (res.code === 200) {
              return {
                promotion: res.data
              };
            }
            return {promotion: null, error: res.message};
          }
        ),
        catchError(err => {
          const message = `Retrieval error ${err}`;
          return of({promotion: null, error: message});
        })
      );
  }
}
