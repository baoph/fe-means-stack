import {Component, ViewChild} from '@angular/core';
import {OwlDateTimeComponent} from '../projects/picker/src/public_api';
import { Moment } from 'moment';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

}
