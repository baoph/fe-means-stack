import {Component, OnDestroy, OnInit} from '@angular/core';
import {DocumentService} from '../../shared/services/http/document.service';
import {Subscription} from 'rxjs';
import {Document} from '../../shared/models/document';
import {startWith} from 'rxjs/operators';
import {MessageService} from '../../shared/services/http/message.service';
import {AuthService} from '../../shared/services/http/auth.service';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit, OnDestroy {
  document: Document;
  private docSub: Subscription;

  constructor(private documentService: DocumentService,
              private messageService: MessageService,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.docSub = this.documentService.currentDocument.pipe(
      startWith({id: '', doc: 'Select an existing document or create a new one to get started'})
    ).subscribe(document => this.document = document);
  }

  ngOnDestroy() {
    this.docSub.unsubscribe();
  }

  editDoc() {
    this.documentService.editDocument(this.document);
  }

  createMessage(content: string) {
    const message = {
      message: content,
      sender: this.authService.getLoggedInUser()._id,
      receiver: '5dc1977e913b8b5f7402de66',
    };
    // this.messageService.newMessage(message).subscribe();
  }
}
