import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { DocumentService } from '../../shared/services/http/document.service';
import {Chat} from '../../shared/models/chat';
import {AuthService} from '../../shared/services/http/auth.service';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit, OnDestroy {
  documents: Observable<string[]>;
  currentDoc: string;
  private docSub: Subscription;

  constructor(private documentService: DocumentService,
              private authService: AuthService) { }

  ngOnInit() {
    this.documentService.getChatsByUserId(this.authService.getLoggedInUser()._id).subscribe();
    this.documents = this.documentService.documents;
    this.docSub = this.documentService.currentDocument.subscribe(doc => this.currentDoc = doc.id);
  }

  ngOnDestroy() {
    this.docSub.unsubscribe();
  }

  loadDoc(id: string) {
    this.documentService.getDocument(id);
  }

  newDoc() {
    const chat = {
      user: this.authService.getLoggedInUser()._id,
      restaurant: '5dc1977e913b8b5f7402de66'
    }
    this.documentService.newDocument(chat).subscribe();
  }

}
