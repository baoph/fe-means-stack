import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ComponentsModule} from './shared/components';
import {ContainersModule} from './shared/containers';
import {HttpServiceModule} from './shared/services/http/http.module';
import {AuthModule} from './features/_core/auth/auth.module';
import {HomeComponent} from './features/_core/components/home/home.component';
import {ContactComponent} from './features/_core/components/contact/contact.component';
import {AboutComponent} from './features/_core/components/about/about.component';
import {RestaurantComponent} from './features/_core/components/restaurant/restaurant.component';
import {StatusDialogComponent} from './features/_core/components/restaurant/status-dialog/status-dialog.component';
import {DemoMaterialModule} from './material-module';
import {MatNativeDateModule} from '@angular/material';
import {ScheduleComponent} from './features/_core/components/schedule/schedule.component';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from '../projects/picker/src/public_api';
import {SubmitComponent} from './features/_core/components/submit/submit.component';
import {OwlMomentDateTimeModule} from '../projects/picker/src/lib/date-time/adapter/moment-adapter/moment-date-time.module';
import {NewSaleComponent} from './features/restaurant-staff/restaurant-sale/new-sale/new-sale.component';
import {CoreLayoutModule} from './shared/containers/layout/core/core-layout.module';
import {PageNotFoundComponent} from './shared/components/page-not-found/page-not-found.component';
import {SocketIoConfig, SocketIoModule} from 'ngx-socket-io';
import {DocumentListComponent} from './components/document-list/document-list.component';
import {DocumentComponent} from './components/document/document.component';

const config: SocketIoConfig = {url: 'http://localhost:80', options: {}};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    AboutComponent,
    ScheduleComponent,
    RestaurantComponent,
    StatusDialogComponent,
    SubmitComponent,
    NewSaleComponent,
    PageNotFoundComponent,
    // socket io
    DocumentListComponent,
    DocumentComponent
  ],
  imports: [
    /** Angular core dependencies */
    BrowserModule,
    FormsModule,
    // NgbModule.forRoot(),
    BrowserAnimationsModule,
    HttpClientModule,
    // MaterialModule,
    DemoMaterialModule,
    MatNativeDateModule,

    /** App custom dependencies */
    ComponentsModule,
    ContainersModule,
    HttpServiceModule.forRoot(),
    AppRoutingModule,
    SlickCarouselModule,
    // SocketIoModule.forRoot(config),
    ReactiveFormsModule,
    AuthModule,
    // InfiniteScrollModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlMomentDateTimeModule,
    CoreLayoutModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [],
  entryComponents: [StatusDialogComponent, NewSaleComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
