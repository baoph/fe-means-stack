import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './features/_core/components/home/home.component';
import {AuthGuard} from './shared/guards/auth.guard';
import {ContactComponent} from './features/_core/components/contact/contact.component';
import {AboutComponent} from './features/_core/components/about/about.component';
import {CoreComponent} from './shared/containers/layout/core/core.component';
import {PageNotFoundComponent} from './shared/components/page-not-found/page-not-found.component';
// import {CourseDialogComponent} from './features/_core/components/course-dialog/course-dialog.component';

const routes: Routes = [

  // Un-protected routes
  {
    path: '', component: CoreComponent, children: [
      {
        path: '', pathMatch: 'full',
        component: HomeComponent,
        // resolve: {resolvedData: RestaurantResolver},
        canActivate: [AuthGuard]
      },
      {
        path: 'contact', pathMatch: 'full',
        component: ContactComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'about', pathMatch: 'full',
        component: AboutComponent,
        canActivate: [AuthGuard]
      },
      {path: '404-not-found', component: PageNotFoundComponent},
      // Restaurant-staff protected routes
      {
        path: 'manage-restaurant',
        loadChildren: () => import('./features/restaurant-staff/restaurant-staff.module').then(m => m.RestaurantStaffModule),
        // canLoad: [ManagerGuard]
      },
    ],
  },
  // User protected routes
  {
    path: '',
    loadChildren: () => import('./features/user/user.module').then(m => m.UserModule),
    // canLoad: [ManagerGuard]
  },
  // {
  //   path: '',
  //   loadChildren: () => import('./features/_core/foundation.module').then(m => m.FoundationModule),
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
