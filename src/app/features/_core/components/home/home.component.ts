import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {map} from 'rxjs/operators';
import {RestaurantService} from '../../../../shared/services/http/restaurant.service';
import {Restaurant} from '../../../../shared/models/restaurant';
import {ActivatedRoute, Router} from '@angular/router';


export interface State {
  flag: string;
  name: string;
  population: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  errorMessage: string;
  stateCtrl = new FormControl();
  restaurants: Restaurant[] = [];
  filteredRestaurants: Observable<Restaurant[]>;

  constructor(private resService: RestaurantService,
              private route: ActivatedRoute,
              private router: Router,
              private restaurantService: RestaurantService) {

  }

  ngOnInit() {
    // this.route.data.subscribe(data => {
    //   const resolvedData: RestaurantResolved = data.resolvedData;
    //   this.errorMessage = resolvedData.error;
    //   console.log(resolvedData.restaurants);
    //   if (resolvedData.restaurants.code === 200 && Array.isArray(resolvedData.restaurants.data)) {
    //     this.onRestaurantRetrieved(resolvedData.restaurants.data);
    //   }
    // });
    this.onRestaurantRetrieved();
    this.filteredRestaurants = this.stateCtrl.valueChanges
      .pipe(
        // startWith(''),
        map(text => {
          return this._filterRestaurants(text);
        })
      );
    // this.articleService.getPublishedArticles().subscribe(
    //     articles => {
    //         this.articles = articles.data;
    //         this.article_next = articles.links.next;
    //     },
    //     error => this.errorMessage = error as any
    // );
    //
    // this.stateServices.scrollObserve.pipe(
    //     debounceTime(400),
    //     distinctUntilChanged(),
    // ).subscribe(scrollTop => {
    //     if (!this.isUpdatingContent && !!this.article_next) {
    //         this.isUpdatingContent = true;
    //         if (!!this.article_next) {
    //             this.articleService.articlePaginate(this.article_next).pipe(
    //                 debounceTime(400),
    //                 distinctUntilChanged())
    //                 .subscribe(articlesNext => {
    //                     console.log('working...');
    //                     this.isUpdatingContent = false;
    //                     this.articles = this.articles.concat(articlesNext.data);
    //                     if (!!articlesNext.links) {
    //                         this.article_next = articlesNext.links.next;
    //                     } else {
    //                         this.article_next = null;
    //                     }
    //                 });
    //         } else {
    //             this.isUpdatingContent = false;
    //         }
    //     }
    //     console.log(scrollTop);
    // });
  }

  onRestaurantRetrieved(): void {
    this.restaurantService.getAllRestaurants().subscribe(
      (res: any) => {
        if (res.code === 200 && Array.isArray(res.data)) {
          this.getRestaurants(res.data);
        }
      });
  }

  getRestaurants(data: Restaurant[]) {
    this.restaurants = data;
  }

  _filterRestaurants(text: string): Restaurant[] {
    const filterValue = text.toLowerCase();
    return this.restaurants.filter(res => res.resName.toLowerCase().includes(filterValue));
  }

  // @HostListener('window:scroll', ['$event'])
  // onScroll(event) {
  //     const windowHeight = event.target.scrollingElement.scrollHeight;
  //     const currentScroll = event.target.scrollingElement.scrollTop;
  //     // console.log(windowHeight);
  //     // console.log(currentScroll);
  //     // console.dir(event.target.scrollingElement);
  //     if ((windowHeight * 0.6) <= currentScroll) {
  //         this.stateServices.onScroll(event.target.scrollingElement.scrollTop);
  //     }
  //     // console.log(event.target.scrollingElement.scrollTop);
  // }
}
