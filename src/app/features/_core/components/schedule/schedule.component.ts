import {Component, OnInit, ViewChild} from '@angular/core';

// import dayGridPlugin from '@fullcalendar/daygrid';
// import {OptionsInput} from '@fullcalendar/core';
// import {FullCalendarComponent} from '@fullcalendar/angular';
// import timeGrigPlugin from '@fullcalendar/timegrid';
// import interactionPlugin from '@fullcalendar/interaction';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../../shared/services/http/auth.service';
import {Schedule} from '../../../../shared/models/schedule';
import {Evt} from '../../../../shared/models/evt';
import {ScheduleService} from '../../../../shared/services/http/schedule.service';


@Component({
    selector: 'app-schedule',
    templateUrl: './schedule.component.html',
    styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent {
    // options: OptionsInput;
    // calendarEvents = [];
    // eventsModel: any;
    // start = '07:30:00';
    // end = '21:30:00';
    // slot = '00:30:00';
    // sub: Subscription;
    // schedules: Schedule[];
    // @ViewChild('fullcalendar', {static: false}) fullcalendar: FullCalendarComponent;
    //
    // constructor(private route: ActivatedRoute,
    //             private authService: AuthService,
    //             private scheduleService: ScheduleService) {
    // }
    //
    //
    // ngOnInit() {
    //     this.options = {
    //         editable: true,
    //         header: {
    //             left: 'prev,next today myCustomButton',
    //             center: 'title',
    //             right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
    //         },
    //         plugins: [dayGridPlugin, timeGrigPlugin, interactionPlugin]
    //     };
    //     // const resolvedScheduleList: CourseScheduleResolved = this.route.snapshot.data.resolvedSchedule;
    //     // this.schedules = resolvedScheduleList.schedules;
    //     // if (this.authService.getRole() === 4) {
    //     this.getStudentSchedule();
    //     // console.log(this.schedules);
    //     // } else if (this.authService.getRole() === 3) {
    //     //     this.getTeacherSchedule();
    //     // }
    //     console.log(this.schedules);
    //     // Assign the data to the data source for the table to render
    //     // this.getEvents(this.schedules);
    // }
    //
    // getStudentSchedule() {
    //     console.log(this.authService.getUserId().toString());
    //     return this.scheduleService.getStudentSchedule(this.authService.getUserId().toString()).subscribe(
    //         data => {
    //             this.schedules = data,
    //                 this.getEvents(this.schedules);
    //         }
    //     );
    // }
    //
    // getTeacherSchedule() {
    //     return this.scheduleService.getTeacherSchedule(this.authService.getUserId().toString()).subscribe(
    //         data => this.schedules = data
    //     );
    // }
    //
    // getEvents(schedules: Schedule[]) {
    //     schedules.forEach(schedule => {
    //         console.log(schedule.start_date);
    //         const e: Evt = {
    //             title: '',
    //             start: '',
    //             end: ''
    //         };
    //         e.start = schedule.start_date;
    //         e.end = schedule.end_date;
    //         e.title = schedule.subjectName + ' at ' + schedule.roomName;
    //
    //         this.calendarEvents.push(e);
    //         console.log(this.calendarEvents);
    //     });
    // }
    //
    // eventClick(model) {
    //     console.log(model);
    // }
    //
    // eventDragStop(model) {
    //     console.log(model);
    // }
    //
    // dateClick(model) {
    //     console.log(model);
    // }
}
