import {AfterViewInit, Component, ElementRef, OnInit, ViewChildren} from '@angular/core';
import {FormBuilder, FormControlName, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, fromEvent, Observable, Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {User} from '../../../../../shared/models/user';
import {GenericValidator} from '../../../../../shared/utils/generic-validator';
import {AuthService} from '../../../../../shared/services/http/auth.service';
import {StateObseverService} from '../../../../../shared/services/tools/state-obsever.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {

    @ViewChildren(FormControlName, {read: ElementRef}) formInputElements: ElementRef[];
    loginForm: FormGroup;
    errorMessage: string;
    user: User;
    // Use with the generic validation message class
    displayMessage: { [key: string]: string } = {};
    private sub: Subscription;
    private readonly validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;
    private componentDestroy: BehaviorSubject<boolean>;

    constructor(private fb: FormBuilder,
                private authService: AuthService,
                private router: Router,
                private roleData: StateObseverService) {
        // email: 'This field must in email format',
        this.validationMessages = {
            email: {
                email: 'Enter a valid email address',
                required: 'Enter your e-mail',
                // minlength: 'E-mail must be at least 6 characters',
                maxlength: 'Email cannot exceed 50 characters'
            },
            password: {
                required: 'Enter your password',
                minlength: 'Password must be at least 6 characters',
                maxlength: 'Password cannot exceed 50 characters'
            }
        };

        // Define an instance of the validator for use with this form,
        // passing in this form's set of validation messages.
        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    get getEmail() {
        return this.loginForm.get('email');
    }

    get getPassword() {
        return this.loginForm.get('password');
    }

    ngOnInit() {
        this.loginForm = this.fb.group({
            email: ['', [
                Validators.required,
                Validators.maxLength(50),
                Validators.email
            ]],
            password: ['', [
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(50)
            ]]
        });


    }

    login(): void {
        if (this.loginForm.valid) {
            const u = {...this.user, ...this.loginForm.value};
            this.authService.login(u)
                .subscribe(
                    (res: any) => {
                        if (res.code === 200) {
                            this.authService.setSession(res);
                            this.onSaveComplete();
                        } else {
                            this.errorMessage = res.msg;
                        }
                    }
                );

        } else {
            this.errorMessage = 'Please correct the validation errors';
        }
    }

    onSaveComplete(): void {
        console.log('UserRouting is logged in');
        this.router.navigate(['/']);
    }

    controlBlurs(): Observable<any>[] {
        return this.formInputElements
            .map(
                (formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur')
            );
    }

    // Nam nó xui t làm cái này xong nó quên cách làm =))
    // ngOnDestroy(): void {
    //   this.componentDestroy.next(true);
    //   this.componentDestroy.complete();
    // }

    ngAfterViewInit(): void {
        if (this.getEmail.value.lenth >= 6) {
            this.getEmail.valueChanges
                .subscribe(checkedValue => {
                    if (checkedValue) {
                        checkedValue.setValidators(Validators.email);
                    } else {
                        checkedValue.clearValidators();
                    }
                    checkedValue.updateValueAndValidity();
                });
        }


        // Watch for the blur event from any input element on the form.
        // This is required because the valueChanges does not provide notification on blur
        this.controlBlurs().forEach(e => {
            e.subscribe(value => {
                this.displayMessage = this.genericValidator.processMessages(this.loginForm);
                console.log(this.displayMessage);
            });
        });
    }
}
