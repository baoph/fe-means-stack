import {AfterViewInit, Component, ElementRef, OnInit, ViewChildren} from '@angular/core';
import {FormBuilder, FormControlName, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../../../shared/models/user';
import {fromEvent, Observable} from 'rxjs';

import {Router} from '@angular/router';
import {GenericValidator} from '../../../../../shared/utils/generic-validator';
import {AuthService} from '../../../../../shared/services/http/auth.service';
import {passwordMatcher} from '../../../../../shared/utils/password.validator';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, AfterViewInit {
  @ViewChildren(FormControlName, {read: ElementRef}) formInputElements: ElementRef[];
  loginForm: FormGroup;
  errorMessage: string;
  user: User;
  // Use with the generic validation message class
  displayMessage: { [key: string]: string } = {};
  // private sub: Subscription;
  private readonly validationMessages: { [key: string]: { [key: string]: string } };
  private genericValidator: GenericValidator;
  // private componentDestroy: BehaviorSubject<boolean>;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router) {
    // email: 'This field must in email format',
    this.validationMessages = {
      email: {
        email: 'Enter a valid email address',
        required: 'Enter your e-mail',
        minlength: 'E-mail must be at least 6 characters',
        maxlength: 'Email cannot exceed 50 characters'
      },
      username: {
        required: 'Enter your username',
        minlength: 'Username must be at least 6 characters',
        maxlength: 'Username cannot exceed 50 characters'
      },
      password: {
        required: 'Enter your password',
        minlength: 'Password must be at least 5 characters',
        maxlength: 'Password cannot exceed 50 characters'
      },
      confirmPassword: {
        required: 'Enter your confirm password',
        minlength: 'Password must be at least 5 characters',
        maxlength: 'Password cannot exceed ed 50 characters',
      }
    };
    // Define an instance of the validator for use with this form,
    // passing in this form's set of validation messages.
    this.genericValidator = new GenericValidator(this.validationMessages);
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50),
        Validators.email]],
      username: ['', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50)]],
      password: ['', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50)]],
      confirmPassword: ['', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50)]],
    }, {validator: passwordMatcher});
  }

  login(): void {
    if (this.loginForm.valid) {
      const u = {...this.user, ...this.loginForm.value};
      this.authService.login(u)
        .subscribe(
          () => this.onSaveComplete()
        );

    } else {
      this.errorMessage = 'Please correct the validation errors';
    }
  }

  onSaveComplete(): void {
    console.log('UserRouting is logged in');
    this.router.navigateByUrl('articles');
  }

  controlBlurs(): Observable<any>[] {
    return this.formInputElements
      .map(
        (formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur')
      );
  }

  // Nam nó xui t làm cái này xong nó quên cách làm =))
  // ngOnDestroy(): void {
  //   this.componentDestroy.next(true);
  //   this.componentDestroy.complete();
  // }

  ngAfterViewInit(): void {
    // Watch for the blur event from any input element on the form.
    // This is required because the valueChanges does not provide notification on blur
    this.controlBlurs().forEach(e => {
      e.subscribe(value => {
        this.displayMessage = this.genericValidator.processMessages(this.loginForm);
        console.log(this.displayMessage);
      });
    });
  }

}
