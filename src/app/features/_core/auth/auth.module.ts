import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ReactiveFormsModule} from '@angular/forms';
import {RegisterComponent} from './components/register/register.component';
import {LoginComponent} from './components/login/login.component';
import {AuthRoutingModule} from './auth-routing.module';
import {MaterialModule} from '../../../shared/material/material.module';


@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        AuthRoutingModule,
        ReactiveFormsModule,
    ],
    declarations: [LoginComponent, RegisterComponent],
    exports: [
        LoginComponent
    ],
    providers: []
})
export class AuthModule {
}
