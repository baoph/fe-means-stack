import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantArticleComponent } from './restaurant-article.component';

describe('RestaurantArticleComponent', () => {
  let component: RestaurantArticleComponent;
  let fixture: ComponentFixture<RestaurantArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurantArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
