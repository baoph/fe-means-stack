import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChildren} from '@angular/core';
import {FormBuilder, FormControlName, FormGroup, Validators} from '@angular/forms';
import {GenericValidator} from '../../../shared/utils/generic-validator';
import {Script} from '../../../shared/services/http/script.service';
import {Article} from '../../../shared/models/article';
import {ActivatedRoute, Router} from '@angular/router';
import {fromEvent, merge, Observable, Subscription} from 'rxjs';
import {debounceTime, map} from 'rxjs/operators';
import {ArticleService} from '../../../shared/services/http/article.service';
import {Utilities} from '../../../shared/services/tools/Convert';
import {AuthService} from '../../../shared/services/http/auth.service';
import {Topic} from '../../../shared/models/topic';
import {RestaurantEditResolved} from '../../../shared/models/restaurant';
import {Category} from '../../../shared/models/category';
import {CategoryService} from '../../../shared/services/http/category.service';

@Component({
  selector: 'app-restaurant-article',
  templateUrl: './restaurant-article.component.html',
  styleUrls: ['./restaurant-article.component.scss']
})
export class RestaurantArticleComponent implements OnInit, AfterViewInit {
  @ViewChildren(FormControlName, {read: ElementRef}) formInputElements: ElementRef[];
  message: string;
  // Pop-up
  isPopup = false;
  popupTitle = '';
  popupContent = '';
  popupDecide = true;
  popupDecideOkText = '';
  popupDecideDeclineText = '';
  // upload
  isUploadEmpty = false;
  isUploadMultiple = false;
  isUploadTooLarge = false;
  isExtensionNotSupported = false;
  fileUpload: File = null;
  // model
  article: Article;
  topics: Observable<Topic[]>;
  submitForm: FormGroup;
  errorMessage: string;
  pageTitle: string;
  createdBy = localStorage.getItem('userId');
  departmentId = localStorage.getItem('departmentId');
  // Use with the generic validation message class
  displayMessage: { [key: string]: string } = {};
  cats: Observable<Category[]>;
  imageArr: string[];
  private sub: Subscription;
  private readonly validationMessages: { [key: string]: { [key: string]: string } };
  private genericValidator: GenericValidator;

  constructor(private script: Script,
              private route: ActivatedRoute,
              private articleService: ArticleService,
              private fb: FormBuilder,
              private cd: ChangeDetectorRef,
              private router: Router,
              private catService: CategoryService,
              private authService: AuthService) {
    this.validationMessages = {
      articleContent: {
        required: 'Content is required.',
        minlength: 'Content must be at least three characters.',
        maxlength: 'Content cannot exceed 2000 characters.'
      },
      articleId: {
        required: 'Article ID is required.',
      },
      category: {
        required: 'Category is required.',
      },
    };

    // Define an instance of the validator for use with this form,
    // passing in this form's set of validation messages.
    this.genericValidator = new GenericValidator(this.validationMessages);
  }

  get content() {
    return this.submitForm.get('articleContent');
  }

  get articleId() {
    return this.submitForm.get('articleId');
  }

  get category() {
    return this.submitForm.get('category');
  }

  get image() {
    return this.submitForm.get('articleImage');
  }

  get currentImage() {
    return this.submitForm.get('currentImage');
  }

  ngOnInit() {
    this.submitForm = this.fb.group({
      articleContent: ['', [Validators.required, Validators.minLength(3)]],
      articleImage: [''],
      currentImage: [''],
      articleId: ['', Validators.required],
      category: ['', Validators.required]
    });

    this.cats = this.catService.getAllCategories().pipe(
      map((cat: any) => {
        console.log(cat);
        return cat.data;
      })
    );
    this.cats.subscribe();
    this.route.data.subscribe(data => {
      const resolvedData: RestaurantEditResolved = data.resolvedData;
      this.errorMessage = resolvedData.error;
      if (Array.isArray(resolvedData.restaurant) && Object.keys(resolvedData.restaurant[0]).length > 0) {
        this.onRestaurantRetrieved(resolvedData.restaurant[0].article);
      }
    });
  }

  ngAfterViewInit(): void {
    // Watch for the blur event from any input element on the form.
    // This is required because the valueChanges does not provide notification on blur
    const controlBlurs: Observable<any>[] = this.formInputElements
      .map(
        (formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur')
      );
    // Merge the blur event observable with the valueChanges observable
    // so we only need to subscribe once.
    merge(this.submitForm.valueChanges, ...controlBlurs).pipe(
      debounceTime(800)
    ).subscribe(value => {
      this.displayMessage = this.genericValidator.processMessages(this.submitForm);
      console.log(this.displayMessage);
    });
  }


  saveArticle(): void {
    console.log(this.image.value);
    if (this.submitForm.valid) {
      const formData = {
        _id: this.article._id,
        articleContent: this.content.value,
        articleImage: this.currentImage.value,
        priceRange: this.article.priceRange,
        category: this.article.category._id,
        images: this.image.value,
      };
      this.articleService.uploadArticle(formData).subscribe(
        (res: any) => {
          if (res.code !== 200) {
            window.alert(res.msg);
          } else {
            window.alert('Submit article successfully!');
            this.onSaveComplete();
          }
        }
      );
    } else {
      this.errorMessage = 'Please correct the validation errors';
    }
  }

  listenFileUploading(event) {
    const reader = new FileReader();
    if (!!!event.target.files) {
      this.errorMessage = 'Please choose image to upload';
      return;
    }
    if (event.target.files.length !== 1) {
      this.errorMessage = 'Please upload a single image';
      return;
    }
    if (event.target.files[0].size >= 7082721) {
      this.errorMessage = 'A uploaded image is too large, please upload again';
      return;
    }
    if (Utilities.checkingExtensionImages(event.target.files[0])) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.image.setValue(file);
        this.image.markAsDirty();
        this.image.updateValueAndValidity();
        this.cd.markForCheck();
      };
    } else {
      this.errorMessage = 'You have been uploaded wrong format of images, please upload again';
    }
  }

  removeImage(selectedImage: string) {
    const newImages = [];
    const currentImages = this.currentImage.value.split(',');
    if (currentImages !== 1) {
      this.currentImage.value.split(',').forEach(img => {
        if (!img.includes(selectedImage)) {
          newImages.push(img);
        }
      });
    }
    this.setCurrentImages(newImages);
    this.imageArr = [...newImages];
  }

  private onRestaurantRetrieved(article: Article) {
    this.article = article;
    this.content.setValue(article.articleContent);
    this.articleId.setValue(article._id);
    this.category.setValue(article.category._id);
    this.imageArr = [...article.articleImage];
    this.setCurrentImages(article.articleImage);
  }

  private setCurrentImages(currentImages: any) {
    if (Array.isArray(currentImages) && currentImages.length > 1) {
      let images = '';
      currentImages.forEach(img => {
        images = images === '' ? img : images + ',' + img;
      });
      this.currentImage.setValue(images);
    } else if (Array.isArray(currentImages) && currentImages.length === 1) {
      this.currentImage.setValue(currentImages[0]);
    } else if (Array.isArray(currentImages)) {
      this.currentImage.setValue([]);
    }
  }

  private onSaveComplete() {
    window.location.reload();
  }
}
