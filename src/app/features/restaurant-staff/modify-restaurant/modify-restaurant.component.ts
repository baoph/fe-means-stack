import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChildren} from '@angular/core';
import {FormBuilder, FormControlName, FormGroup, Validators} from '@angular/forms';
import {Restaurant, RestaurantEditResolved} from '../../../shared/models/restaurant';
import {fromEvent, merge, Observable, Subscription} from 'rxjs';
import {GenericValidator} from '../../../shared/utils/generic-validator';
import {RestaurantService} from '../../../shared/services/http/restaurant.service';
import {ActivatedRoute} from '@angular/router';
import {StatusService} from '../../../shared/services/http/status.service';
import {debounceTime} from 'rxjs/operators';
import {AuthService} from '../../../shared/services/http/auth.service';
import {AlertService} from '../../../shared/services/tools/alert.service';

@Component({
  selector: 'app-modify-restaurant',
  templateUrl: './modify-restaurant.component.html',
  styleUrls: ['./modify-restaurant.component.scss']
})
export class ModifyRestaurantComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChildren(FormControlName, {read: ElementRef}) formInputElements: ElementRef[];
  restaurant: Restaurant;
  errorMessage: string;
  restaurantId: string;
  // Use with the generic validation message class
  displayMessage: { [key: string]: string } = {};
  isManager = false;
  private sub: Subscription;
  private readonly validationMessages: { [key: string]: { [key: string]: string } };
  private genericValidator: GenericValidator;
  private restaurantForm: FormGroup;

  constructor(private restaurantService: RestaurantService,
              private route: ActivatedRoute,
              private authService: AuthService,
              private statusService: StatusService,
              private fb: FormBuilder,
              private alertService: AlertService) {
    this.validationMessages = {
      username: {
        required: 'Username is required.',
        minlength: 'Username must be at least 6 characters.',
        maxlength: 'Username cannot exceed 50 characters.'
      },
      tel: {
        required: 'Phone number is required.',
        number: 'Phone number must be number',
        minlength: 'Phone number must be at least 10 numbers.',
        maxlength: 'Phone cannot exceed 11 numbers'
      },
      email: {
        email: 'Enter a valid email address',
        required: 'Enter your e-mail',
        maxlength: 'Email cannot exceed 50 characters'
      },
      resName: {
        resName: 'Restaurant\'s name is required.',
        minlength: 'Restaurant\'s name must be at least 6 characters.',
        maxlength: 'Restaurant\'s name cannot exceed 100 characters.'
      },
      resAddress: {
        resName: 'Restaurant\'s address is required.',
        minlength: 'Restaurant\'s address must be at least 6 characters.',
        maxlength: 'Restaurant\'s address cannot exceed 150 characters.'
      }
    };

    // Define an instance of the validator for use with this form,
    // passing in this form's set of validation messages.
    this.genericValidator = new GenericValidator(this.validationMessages);

  }

  get username() {
    return this.restaurantForm.get('articleTitle');
  }

  get tel() {
    return this.restaurantForm.get('tel');
  }

  get email() {
    return this.restaurantForm.get('email');
  }

  get resName() {
    return this.restaurantForm.get('resName');
  }

  get resAddress() {
    return this.restaurantForm.get('resAddress');
  }

  ngOnInit() {
    this.restaurantForm = this.fb.group({
      restaurantId: [this.restaurantId],
      username: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]],
      tel: ['', [Validators.required, Validators.minLength(10)], Validators.maxLength(11)],
      email: ['', Validators.required],
      resName: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(100)]],
      resAddress: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(150)]],
    });

    this.route.data.subscribe(data => {
      const resolvedData: RestaurantEditResolved = data.resolvedData;
      this.errorMessage = resolvedData.error;
      if (Array.isArray(resolvedData.restaurant) && Object.keys(resolvedData.restaurant[0]).length > 0) {
        this.onRestaurantRetrieved(resolvedData.restaurant[0]);
      }
    });
    // this.sub = this.route.paramMap
    //   .subscribe(
    //     params => {
    //       const id = params.get('id');
    //       this.restaurantId = id;
    //     }
    //   );
  }

  ngAfterViewInit(): void {
    // Watch for the blur event from any input element on the form.
    // This is required because the valueChanges does not provide notification on blur
    const controlBlurs: Observable<any>[] = this.formInputElements
      .map(
        (formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur')
      );
    // Merge the blur event observable with the valueChanges observable
    // so we only need to subscribe once.
    merge(this.restaurantForm.valueChanges, ...controlBlurs).pipe(
      debounceTime(800)
    ).subscribe(value => {
      this.displayMessage = this.genericValidator.processMessages(this.restaurantForm);
      console.log(this.displayMessage);
    });
  }

  save(): void {
    if (this.restaurantForm.valid) {
      if (this.restaurantForm.dirty) {
        this.restaurantService.updateRestaurant(this.restaurant)
          .subscribe(
            (res: any) => {
              if (res.code !== 200) {
                this.alertService.openAlertDialog(res.code, res.message);
              } else {
                this.alertService.openAlertDialog(res.code, 'Update restaurant successful');
              }
            },
            (error: any) => this.errorMessage = error as any
          );
      } else {
        this.alertService.openAlertDialog(400, 'Invalid validate');
      }
    } else {
      this.errorMessage = 'Please correct the validation errors';
    }
  }

  ngOnDestroy(): void {
  }

  onRestaurantRetrieved(restaurant: Restaurant): void {
    Object.keys(restaurant).forEach(key => {
          if (!!restaurant[key] && (key !== 'article' && key !== 'updated' && key !== '_id' && key !== 'status')) {
            this.restaurantForm.get(key).setValue(restaurant[key]);
          }
        });
    this.restaurant = restaurant;
  }
}
