import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModifyRestaurantComponent} from './modify-restaurant/modify-restaurant.component';
import {ManageRestaurantComponent} from './manage-restaurant/manage-restaurant.component';
import {RestaurantStaffRouting} from './restaurant-staff.routing';
import {MaterialModule} from '../../shared/material/material.module';
import { RestaurantSaleComponent } from './restaurant-sale/restaurant-sale.component';
import { RestaurantArticleComponent } from './restaurant-article/restaurant-article.component';
import {ReactiveFormsModule} from '@angular/forms';
import { NewSaleComponent } from './restaurant-sale/new-sale/new-sale.component';
import {OwlDateTimeModule} from '../../../projects/picker/src/lib/date-time/date-time.module';
import { ManageChatComponent } from './manage-chat/manage-chat.component';

@NgModule({
  declarations: [
    ModifyRestaurantComponent,
    ManageRestaurantComponent,
    RestaurantSaleComponent,
    RestaurantArticleComponent,
    ManageChatComponent,
  ],
  imports: [
    CommonModule,
    RestaurantStaffRouting,
    MaterialModule,
    ReactiveFormsModule,
    OwlDateTimeModule
  ]
})
export class RestaurantStaffModule { }
