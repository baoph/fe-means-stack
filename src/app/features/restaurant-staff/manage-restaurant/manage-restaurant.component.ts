import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChildren} from '@angular/core';
import {fromEvent, merge, Observable, Subscription} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {FormBuilder, FormControlName, FormGroup, Validators} from '@angular/forms';
import {GenericValidator} from '../../../shared/utils/generic-validator';
import {Script} from '../../../shared/services/http/script.service';
import {Article} from '../../../shared/models/article';
import {ActivatedRoute, Router} from '@angular/router';
import {TopicService} from '../../../shared/services/http/topic.service';
import {ArticleService} from '../../../shared/services/http/article.service';
import {Utilities} from '../../../shared/services/tools/Convert';
import {AuthService} from '../../../shared/services/http/auth.service';
import {Topic} from '../../../shared/models/topic';

@Component({
  selector: 'app-manage-restaurant',
  templateUrl: './manage-restaurant.component.html',
  styleUrls: ['./manage-restaurant.component.scss']
})
export class ManageRestaurantComponent implements OnInit, AfterViewInit {

  @ViewChildren(FormControlName, {read: ElementRef}) formInputElements: ElementRef[];
  message: string;
  // Pop-up
  isPopup = false;
  popupTitle = '';
  popupContent = '';
  popupDecide = true;
  popupDecideOkText = '';
  popupDecideDeclineText = '';
  // upload
  isUploadEmpty = false;
  isUploadMultiple = false;
  isUploadTooLarge = false;
  isExtensionNotSupported = false;
  fileUpload: File = null;
  // model
  article: Article;
  topics: Observable<Topic[]>;
  submitForm: FormGroup;
  errorMessage: string;
  pageTitle: string;
  createdBy = localStorage.getItem('userId');
  departmentId = localStorage.getItem('departmentId');
  // Use with the generic validation message class
  displayMessage: { [key: string]: string } = {};
  private sub: Subscription;
  private readonly validationMessages: { [key: string]: { [key: string]: string } };
  private genericValidator: GenericValidator;

  constructor(private script: Script,
              private route: ActivatedRoute,
              private articleService: ArticleService,
              private fb: FormBuilder,
              private cd: ChangeDetectorRef,
              private router: Router,
              private topicService: TopicService,
              private authService: AuthService) {
    this.validationMessages = {
      articleTitle: {
        required: 'Title is required.',
        minlength: 'Title must be at least three characters.',
        maxlength: 'Title cannot exceed 50 characters.'
      },
      articleContent: {
        required: 'Content is required.',
        minlength: 'Content must be at least three characters.',
        maxlength: 'Content cannot exceed 2000 characters.'
      },
      topic_id: {
        required: 'Topic is required.',
      },
      department_id: {
        required: 'Department is required'
      },
      created_by: {
        required: 'Create by is required'
      }
    };

    // Define an instance of the validator for use with this form,
    // passing in this form's set of validation messages.
    this.genericValidator = new GenericValidator(this.validationMessages);
  }

  get title() {
    return this.submitForm.get('articleTitle');
  }

  get content() {
    return this.submitForm.get('articleContent');
  }

  get topic() {
    return this.submitForm.get('topic_id');
  }

  get department() {
    return this.submitForm.get('department_id');
  }

  get image() {
    return this.submitForm.get('articleImage');
  }

  get created_by() {
    return this.submitForm.get('created_by');
  }

  ngOnInit() {
    this.submitForm = this.fb.group({
      articleTitle: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      articleContent: ['', [Validators.required, Validators.minLength(3)]],
      articleImage: [null, Validators.required],
      topic_id: [0, Validators.required],
      department_id: [1, Validators.required],
      created_by: [1, Validators.required]
    });
    this.topics = this.getTopics();

  }

  getTopics() {
    return this.topicService.getTopics();
  }

  ngAfterViewInit(): void {
    // this.script.load('editor', 'droptify')
    //     .then((data: any) => {
    //         console.log(data, 'script loaded');
    //     })
    //     .catch((error) => {
    //     });
    // Watch for the blur event from any input element on the form.
    // This is required because the valueChanges does not provide notification on blur
    const controlBlurs: Observable<any>[] = this.formInputElements
      .map(
        (formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur')
      );
    // Merge the blur event observable with the valueChanges observable
    // so we only need to subscribe once.
    merge(this.submitForm.valueChanges, ...controlBlurs).pipe(
      debounceTime(800)
    ).subscribe(value => {
      this.displayMessage = this.genericValidator.processMessages(this.submitForm);
      console.log(this.displayMessage);
    });
  }


  saveArticle(): void {
    // if (this.submitForm.valid) {
    //     if (this.submitForm.dirty) {
    // const formData = {
    //     articleTitle: this.title.value,
    //     articleContent: this.content.value,
    //     articleImage: this.image.value,
    //     topic_id: this.topic.value,
    //     created_by: this.authService.getUserId()
    // };
    // this.articleService.uploadArticle(formData).subscribe(
    //     res => {
    //         if (res.error) {
    //             window.alert(res.error);
    //         } else {
    //             window.alert('Submit article successfully!');
    //             this.onSaveComplete();
    //         }
    //     }
    // );
    // } else {
    //     this.onSaveComplete();
    // }
    // }
    // else {
    //     this.errorMessage = 'Please correct the validation errors';
    // }
  }

  setup_popupDisplay(title: string, content: string, isDecide: boolean, okText: string, noText: string) {
    this.popupContent = content;
    this.popupTitle = title;
    this.popupDecide = isDecide;
    this.popupDecideOkText = okText;
    this.popupDecideDeclineText = noText;
    this.isPopup = true;
  }

  // saveArticle(): void {
  //     if (this.submitForm.valid) {
  //         if (this.submitForm.dirty) {
  //             const p = {...this.article, ...this.submitForm.value};
  //             this.articleService.createArticle(p)
  //                 .subscribe(
  //                     () => this.onSaveComplete(),
  //                     (error: any) => this.errorMessage = error as any
  //                 );
  //         } else {
  //             this.onSaveComplete();
  //         }
  //     } else {
  //         this.errorMessage = 'Please correct the validation errors';
  //     }
  // }

  onSaveComplete(): void {
    // Reset the form to clear the flags
    this.submitForm.reset();
    this.router.navigate(['/articles']);
  }

  listenFileUploading(event) {
    const reader = new FileReader();
    if (!!!event.target.files) {
      this.isUploadEmpty = true;
      return;
    }
    if (event.target.files.length !== 1) {
      this.isUploadMultiple = true;
      return;
    }
    if (event.target.files[0].size >= 7082721) {
      this.isUploadTooLarge = true;
      return;
    }
    if (Utilities.checkingExtensionImages(event.target.files[0])) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.fileUpload = file;
        this.image.setValue(file);
        this.image.markAsDirty();
        this.image.updateValueAndValidity();
        this.cd.markForCheck();
      };
      console.log(this.image);
    } else {
      this.isExtensionNotSupported = true;
    }
  }

}
