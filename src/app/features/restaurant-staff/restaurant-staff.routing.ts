import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ManageRestaurantComponent} from './manage-restaurant/manage-restaurant.component';
import {ModifyRestaurantComponent} from './modify-restaurant/modify-restaurant.component';
import {RestaurantArticleComponent} from './restaurant-article/restaurant-article.component';
import {RestaurantSaleComponent} from './restaurant-sale/restaurant-sale.component';
import {RestaurantEditResolver} from '../../shared/resolvers/restaurant/restaurant-edit-resolve.service';
import {PromotionResolver} from '../../shared/resolvers/promotion/promotion-resolver.service';
import {ManageChatComponent} from './manage-chat/manage-chat.component';


const routes: Routes = [
  // {
  // path: '', component: CoreComponent, children: [
  {
    path: '',
    component: ManageRestaurantComponent,
    // canActivate: [AuthGuard],
    children: [
      {
        path: 'modify',
        component: ModifyRestaurantComponent,
        resolve: {resolvedData: RestaurantEditResolver},
      },
      {
        path: 'article',
        component: RestaurantArticleComponent,
        resolve: {resolvedData: RestaurantEditResolver},
        // canActivate: [AuthGuard]
      },
      {
        path: 'sale',
        component: RestaurantSaleComponent,
        resolve: {resolvedData: PromotionResolver},
        // canActivate: [AuthGuard]
      },

    ]
  },
  {
    path: 'manage-chat',
    component: ManageChatComponent,
    // canActivate: [AuthGuard]
  },

  // {path: '404-not-found', component: PageNotFoundComponent}
];
// },
// ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestaurantStaffRouting {
}
