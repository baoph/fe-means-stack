import {AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormControlName, FormGroup, Validators} from '@angular/forms';
import {Promotion} from '../../../../shared/models/promotion';
import {OwlDateTimeComponent} from '../../../../../projects/picker/src/lib/date-time/date-time-picker.component';
import {fromEvent, merge, Observable, Subscription} from 'rxjs';
import {GenericValidator} from '../../../../shared/utils/generic-validator';
import {PromotionService} from '../../../../shared/services/http/promotion.service';
import {debounceTime} from 'rxjs/operators';
import {NumberValidators} from '../../../../shared/utils/number.validator';
import {AuthService} from '../../../../shared/services/http/auth.service';

@Component({
  selector: 'app-new-sale',
  templateUrl: './new-sale.component.html',
  styleUrls: ['./new-sale.component.scss']
})
export class NewSaleComponent implements OnInit, AfterViewInit {
  @ViewChildren(FormControlName, {read: ElementRef}) formInputElements: ElementRef[];
  promotion: Promotion;
  errorMessage: string;
  displayMessage: { [key: string]: string } = {};
  proForm: FormGroup;
  pro: Promotion;
  @ViewChild('date_range_component', {static: true})
  dateRangeComponent: OwlDateTimeComponent<NewSaleComponent>;
  private sub: Subscription;
  private readonly validationMessages: { [key: string]: { [key: string]: string } };
  private genericValidator: GenericValidator;

  constructor(
    public dialogRef: MatDialogRef<NewSaleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Promotion,
    private fb: FormBuilder,
    private proService: PromotionService,
    private authService: AuthService
    // private route: ActivatedRoute,
    // private articleData: StateObseverService
  ) {
    this.validationMessages = {
      promotionName: {
        required: 'Name is required.',
        minlength: 'Name must be at least three characters.',
        maxlength: 'Name cannot exceed 150 characters.'
      },
      promotionPercent: {
        required: 'Percent is required.',
        range: 'Percent must be between 1% and 100%.',
      },
      promotionFrom: {
        required: 'Start date is required.',
      },
      promotionTo: {
        required: 'End date is required.',
      },
    };
    // Define an instance of the validator for use with this form,
    // passing in this form's set of validation messages.
    this.genericValidator = new GenericValidator(this.validationMessages);
  }

  get promotionName() {
    return this.proForm.get('promotionName');
  }
  get promotionPercent() {
    return this.proForm.get('promotionPercent');
  }
  get promotionFrom() {
    return this.proForm.get('promotionFrom');
  }
  get promotionTo() {
    return this.proForm.get('promotionTo');
  }
  get restaurant() {
    return this.proForm.get('restaurant');
  }


  ngOnInit(): void {
    this.proForm = this.fb.group({
      promotionName: ['', [Validators.required, Validators.minLength(3),
        Validators.maxLength(150)]],
      promotionPercent: [1, [Validators.required, NumberValidators.range(1, 100)]],
      promotionFrom: ['', Validators.required],
      promotionTo: ['', Validators.required],
      restaurant: ['', Validators.required]
    });
    if (this.isEdit()) {
      this.editFormData();
    } else {
      this.restaurant.setValue(this.authService.getRestaurantId());
    }
  }
  isEdit(): boolean {
    return this.data != null && Object.entries(this.data).length > 0;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngAfterViewInit(): void {
    // Watch for the blur event from any input element on the form.
    // This is required because the valueChanges does not provide notification on blur
    const controlBlurs: Observable<any>[] = this.formInputElements
      .map(
        (formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur')
      );
    // Merge the blur event observable with the valueChanges observable
    // so we only need to subscribe once.
    merge(this.proForm.valueChanges, ...controlBlurs).pipe(
      debounceTime(800)
    ).subscribe(value => {
      this.displayMessage = this.genericValidator.processMessages(this.proForm);
      console.log(this.displayMessage);
    });
  }

  onSaveComplete(): void {
    // Reset the form to clear the flags
    this.proForm.reset();
    this.dialogRef.close();
  }


  savePromotion() {
    if (this.proForm.valid) {
      if (this.proForm.dirty) {
        const p = {...this.pro, ...this.proForm.value};
        if (this.isEdit()) {
          this.proService.editPromotion(p)
            .subscribe(
              (res: any) => {
                if (res.code === 404) {
                  this.errorMessage = 'Promotion not found';
                } else if (res.code === 200) {
                  window.alert('Update new promotion successful');
                  this.onSaveComplete();
                }
              }
            );
        } else {
          this.proService.createPromotion(p)
            .subscribe(
              (res: any) => {
                if (res.code === 404) {
                  this.errorMessage = 'Promotion not found';
                } else if (res.code === 200) {
                  window.alert('Create new promotion successful');
                  this.onSaveComplete();
                }
              }
            );
        }
      } else {
        this.onSaveComplete();
      }
    } else {
      this.errorMessage = 'Please correct the validation errors';
    }
  }

  private editFormData() {
    this.promotionName.setValue(this.data.promotionName);
    this.promotionPercent.setValue(this.data.promotionPercent);
    this.promotionFrom.setValue(this.data.promotionFrom);
    this.promotionTo.setValue(this.data.promotionTo);
    this.restaurant.setValue(this.data.restaurant);
  }
}
