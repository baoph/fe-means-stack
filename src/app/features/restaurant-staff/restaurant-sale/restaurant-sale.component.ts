import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTable, MatTableDataSource} from '@angular/material';
import {Promotion, PromotionResolved} from '../../../shared/models/promotion';
import {Subscription} from 'rxjs';
import {GenericValidator} from '../../../shared/utils/generic-validator';
import {ActivatedRoute, Router} from '@angular/router';
import {DepartmentService} from '../../../shared/services/http/department.service';
import {FormBuilder} from '@angular/forms';
import {StatusDialogComponent} from '../../_core/components/restaurant/status-dialog/status-dialog.component';
import {NewSaleComponent} from './new-sale/new-sale.component';

@Component({
  selector: 'app-restaurant-sale',
  templateUrl: './restaurant-sale.component.html',
  styleUrls: ['./restaurant-sale.component.scss']
})
export class RestaurantSaleComponent implements OnInit {
  dataSource: MatTableDataSource<Promotion>;
  displayedColumns: string[] = ['promotionName', 'promotionPercent', 'promotionFrom', 'promotionTo',
    'detail', 'update', 'delete'
  ];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatTable, {static: true}) table: MatTable<any>;
  // Use with the generic validation message class
  displayMessage: { [key: string]: string } = {};
  private errorMessage: string;
  private promotions: Promotion[];

  constructor(private route: ActivatedRoute,
              private departmentService: DepartmentService,
              private router: Router,
              private fb: FormBuilder,
              private dialog: MatDialog) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      const resolvedData: PromotionResolved = data.resolvedData;
      this.errorMessage = resolvedData.error;
      console.log(resolvedData);
      if (Array.isArray(resolvedData.promotion)) {
        this.onPromotionRetrieved(resolvedData.promotion);
      }
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private onPromotionRetrieved(promotions: Promotion[]) {
    this.promotions = promotions;
    this.dataSource = new MatTableDataSource(promotions);
  }

  openDialog(promotion?: Promotion): void {
    const dialogRef = this.dialog.open(NewSaleComponent, {
      width: '1180px',
      data: promotion
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
