import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantSaleComponent } from './restaurant-sale.component';

describe('RestaurantSaleComponent', () => {
  let component: RestaurantSaleComponent;
  let fixture: ComponentFixture<RestaurantSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurantSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
