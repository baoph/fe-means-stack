import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserRouting} from './user.routing';
import {MaterialModule} from '../../shared/material/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {OwlDateTimeModule} from '../../../projects/picker/src/lib/date-time/date-time.module';
import {RestaurantComponent} from './restaurant/restaurant.component';
import {StatusDialogComponent} from './restaurant/status-dialog/status-dialog.component';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {ChatComponent} from './chat/chat.component';
import {CoreLayoutModule} from '../../shared/containers/layout/core/core-layout.module';

@NgModule({
  declarations: [
    RestaurantComponent,
    StatusDialogComponent,
    ChatComponent
  ],
  imports: [
    CommonModule,
    UserRouting,
    MaterialModule,
    SlickCarouselModule,
    ReactiveFormsModule,
    OwlDateTimeModule,
    CoreLayoutModule,
    FormsModule,
  ]
})
export class UserModule {
}
