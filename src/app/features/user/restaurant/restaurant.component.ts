import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {StatusDialogComponent} from './status-dialog/status-dialog.component';
import {RestaurantService} from '../../../shared/services/http/restaurant.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Restaurant, RestaurantEditResolved} from '../../../shared/models/restaurant';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss']
})
export class RestaurantComponent implements OnInit {
  private sub: Subscription;
  restaurant: Restaurant;
  isOpenChat = false;
  slides = [
    {img: 'https://images.foody.vn/res/g92/919202/s512x320/foody-upload-api-foody-album-58749242_39078773818-190524184237.jpg'},
    {img: 'https://images.foody.vn/res/g92/919202/s512x320/foody-upload-api-foody-album-58902153_39078772485-190524184247.jpg'},
    {img: 'https://images.foody.vn/res/g92/919202/s512x320/foody-upload-api-foody-album-58793250_39078804485-190524184247.jpg'},
    {img: 'https://images.foody.vn/res/g92/919202/s512x320/foody-upload-api-foody-album-58461828_39078802152-190524184228.jpg'},
    {img: 'https://images.foody.vn/res/g92/919202/s512x320/foody-upload-api-foody-album-58604178_39078800152-190524184237.jpg'},
  ];
  slideConfigTop = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  };
  slideConfigBottom = {
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    autoplay: true,
    autoplaySpeed: 2000,
    // centerMode: true,
    focusOnSelect: true
  };

  constructor(private dialog: MatDialog,
              private route: ActivatedRoute,
              private restaurantService: RestaurantService,
              private fb: FormBuilder,
              private router: Router) {
  }

  slickInit(e) {
    console.log('slick initialized');
  }

  ngOnInit() {
    this.sub = this.route.paramMap
      .subscribe(
        params => {
          const id = params.get('id');
          // this.getDepartment(id);
        }
      );
    const resolvedData: RestaurantEditResolved = this.route.snapshot.data.resolvedData;
    if (Array.isArray(resolvedData.restaurant) && Object.keys(resolvedData.restaurant[0]).length > 0) {
      this.onRestaurantRetrieved(resolvedData.restaurant[0]);
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(StatusDialogComponent, {
      width: '1180px',
      data: this.restaurant
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  private onRestaurantRetrieved(restaurant: Restaurant) {
    this.restaurant = restaurant;
  }

  openChatBox() {
    this.isOpenChat = !this.isOpenChat;
  }
}
