import {AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormControlName, FormGroup, Validators} from '@angular/forms';
import {GenericValidator} from '../../../../shared/utils/generic-validator';
import {fromEvent, merge, Observable, Subscription} from 'rxjs';
import {OwlDateTimeComponent} from '../../../../../projects/picker/src/lib/date-time/date-time-picker.component';
import {NumberValidators} from '../../../../shared/utils/number.validator';
import {AuthService} from '../../../../shared/services/http/auth.service';
import {LoggedInUser} from '../../../../shared/models/user';
import {debounceTime, map} from 'rxjs/operators';
import {Order} from '../../../../shared/models/order';
import {OrderService} from '../../../../shared/services/http/order-service';
import {Restaurant} from '../../../../shared/models/restaurant';
import {PromotionService} from '../../../../shared/services/http/promotion.service';
import {Promotion} from '../../../../shared/models/promotion';

/** Error when invalid control is dirty, touched, or submitted. */
// export class MyErrorStateMatcher implements ErrorStateMatcher {
//     isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//         const isSubmitted = form && form.submitted;
//         return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
//     }
// }

export interface NumOfPeople {
  value: any;
  num: any;
}

@Component({
  selector: 'app-status-dialog',
  templateUrl: './status-dialog.component.html',
  styleUrls: ['./status-dialog.component.scss']
})
export class StatusDialogComponent implements OnInit, AfterViewInit {
  @ViewChildren(FormControlName, {read: ElementRef}) formInputElements: ElementRef[];
  errorMessage: string;
  order: Order;
  promotions: Observable<Promotion[]>;
  displayMessage: { [key: string]: string } = {};
  adults: NumOfPeople[] = [];
  user: LoggedInUser;
  children: NumOfPeople[] = [];
  orderForm: FormGroup;
  @ViewChild('date_range_component', {static: true})
  dateRangeComponent: OwlDateTimeComponent<StatusDialogComponent>;
  private sub: Subscription;
  private readonly validationMessages: { [key: string]: { [key: string]: string } };
  private genericValidator: GenericValidator;

  constructor(
    public dialogRef: MatDialogRef<StatusDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Restaurant,
    private fb: FormBuilder,
    private authService: AuthService,
    private orderService: OrderService,
    private proService: PromotionService,
    // private articleService: ArticleService,
    // private route: ActivatedRoute,
    // private articleData: StateObseverService
  ) {
    // Define an instance of the validator for use with this form,
    // passing in this form's set of validation messages.
    this.validationMessages = {
      promotion: {
        required: 'Promotion is required.',
      },
      adult: {
        required: 'Adult number is required',
        range: 'Percent must be between 1 and 100.',
      },
      children: {
        required: 'Children number is required',
        range: 'Percent must be between 0 and 100.',
      },
      orderDate: {
        required: 'Booking date is required.',
      },
      orderTime: {
        required: 'Booking time is required.',
      },
      email: {
        email: 'Enter a valid email address',
        required: 'Enter your e-mail',
        minlength: 'E-mail must be at least 6 characters',
        maxlength: 'Email cannot exceed 50 characters'
      },
      username: {
        required: 'Enter your username',
        minlength: 'Username must be at least 6 characters',
        maxlength: 'Username cannot exceed 50 characters'
      },
      tel: {
        required: 'Enter your phone number',
        minlength: 'Phone number must be at least 10 number',
        maxlength: 'Phone number cannot exceed 11 number'
      },
    };
    // Define an instance of the validator for use with this form,
    // passing in this form's set of validation messages.
    this.genericValidator = new GenericValidator(this.validationMessages);
    for (let i = 1; i <= 100; i++) {
      const numOfAdult = i !== 1 ? i + ' adults' : i + ' adult';
      const numOfChildren = i + ' children';
      this.adults.push({value: i, num: numOfAdult});
      this.children.push({value: i, num: numOfChildren});
    }
  }

  get username() {
    return this.orderForm.get('username');
  }

  get email() {
    return this.orderForm.get('email');
  }

  get tel() {
    return this.orderForm.get('tel');
  }

  get userId() {
    return this.orderForm.get('user');
  }

  get restaurant() {
    return this.orderForm.get('restaurant');
  }


  ngOnInit(): void {
    this.orderForm = this.fb.group({
      promotion: ['', Validators.required],
      orderDate: ['', Validators.required],
      orderTime: ['', Validators.required],
      adult: [1, [Validators.required, NumberValidators.range(1, 100)]],
      children: [0, [Validators.required, NumberValidators.range(0, 100)]],
      user: ['', Validators.required],
      restaurant: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email,
        Validators.minLength(6), Validators.maxLength(50)]],
      tel: ['', Validators.required],
      note: ['']
    });
    this.getRestaurantPromotion();
    this.setUserInformation();
    // this.articleData.currentMessage.subscribe(message => this.articleForm.patchValue({article_id: message}));
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngAfterViewInit(): void {
    // Watch for the blur event from any input element on the form.
    // This is required because the valueChanges does not provide notification on blur
    const controlBlurs: Observable<any>[] = this.formInputElements
      .map(
        (formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur')
      );
    // Merge the blur event observable with the valueChanges observable
    // so we only need to subscribe once.
    merge(this.orderForm.valueChanges, ...controlBlurs).pipe(
      debounceTime(800)
    ).subscribe(value => {
      this.displayMessage = this.genericValidator.processMessages(this.orderForm);
    });
  }


  onSaveComplete(): void {
    // Reset the form to clear the flags
    // this.articleForm.reset();
    this.dialogRef.close();
  }


  sendOrder() {
    if (this.orderForm.valid) {
      if (this.orderForm.dirty) {
        const p = {...this.order, ...this.orderForm.value};
        this.orderService.createOrder(p).subscribe(
          // () => this.onSaveComplete(),
          // (error: any) => this.errorMessage = error as any
        );
      } else {
        this.onSaveComplete();
      }
    } else {
      this.errorMessage = 'Please correct the validation errors';
    }
  }

  private setUserInformation() {
    this.user = this.authService.getLoggedInUser();
    this.username.setValue(this.user.username);
    this.email.setValue(this.user.email);
    this.tel.setValue(this.user.tel);
    this.userId.setValue(this.user._id);
    this.restaurant.setValue(this.data._id);
  }

  private getRestaurantPromotion() {
    this.promotions = this.proService.getPromotionByRestaurant(this.data._id).pipe(
      map(res => {
        const promotions = [...res.data];
        return promotions;
      })
    );
  }
}
