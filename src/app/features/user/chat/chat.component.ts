import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {concat, Observable, Subscription} from 'rxjs';
import {MessageService} from '../../../shared/services/http/message.service';
import {DocumentService} from '../../../shared/services/http/document.service';
import {AuthService} from '../../../shared/services/http/auth.service';
import {map, switchMap, take, tap} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Message} from '../../../shared/models/message';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {
  @Input() restaurantId: string;
  documents: Observable<string[]>;
  currentDoc: string;
  chatForm: FormGroup;
  message: Message;
  chatId: string;
  messages: Observable<Message[]>;
  private docSub: Subscription;

  constructor(private documentService: DocumentService,
              private authService: AuthService,
              private messageService: MessageService,
              private fb: FormBuilder) {
  }

  get messageContent() {
    return this.chatForm.get('message');
  }

  get sender() {
    return this.chatForm.get('sender');
  }

  get receiver() {
    return this.chatForm.get('receiver');
  }

  get currentChat() {
    return this.chatForm.get('chat');
  }

  ngOnInit() {
    this.chatForm = this.fb.group({
      message: ['', Validators.required],
      sender: ['', Validators.required],
      receiver: ['', Validators.required],
      chat: ['', Validators.required],
    });

    // this.documentService.getChatsByUserId(this.authService.getLoggedInUser()._id).subscribe();
    this.documentService.getChatId(this.authService.getLoggedInUser()._id, this.restaurantId).pipe(
      switchMap((res: any) => {
        this.chatId = res.data[0]._id;
        this.currentChat.setValue(this.chatId);
        return this.messageService.getMessagesByChatId(this.chatId).pipe(
          map((msg: any) => {
            return msg.data;
          })
        );
      })
    ).subscribe();

    this.setDefaultValue();

    this.documents = this.documentService.documents;
    this.messages = this.documentService.messages;
    this.docSub = this.documentService.currentDocument.subscribe(doc => this.currentDoc = doc.id);
  }

  ngOnDestroy() {
    this.docSub.unsubscribe();
  }

  loadDoc(id: string) {
    this.documentService.getDocument(id);
  }

  newDoc() {
    const chat = {
      user: this.authService.getLoggedInUser()._id,
      restaurant: '5dc1977e913b8b5f7402de66'
    };
    this.documentService.newDocument(chat).subscribe();
  }

  createMessage(content: string) {
    const message = {
      message: content,
      sender: this.authService.getLoggedInUser()._id,
      receiver: '5dc1977e913b8b5f7402de66',
    };
    // this.messageService.newMessage(message).subscribe();
  }

  newMessage() {
    if (this.chatForm.valid) {
      if (this.chatForm.dirty) {
        const params = {...this.message, ...this.chatForm.value};
        this.messageService.newMessage(params).subscribe(
          () => this.onSaveCompelete()
        );
      }
    }
  }

  private setDefaultValue() {
    this.sender.setValue(this.authService.getLoggedInUser()._id);
    this.receiver.setValue(this.restaurantId);
  }

  private onSaveCompelete() {
    this.messageContent.setValue('');
  }
}
