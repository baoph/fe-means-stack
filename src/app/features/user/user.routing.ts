import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RestaurantComponent} from './restaurant/restaurant.component';
import {AuthGuard} from '../../shared/guards/auth.guard';
import {RestaurantViewResolver} from '../../shared/resolvers/restaurant/restaurant-view-resolver.service';
import {ChatComponent} from './chat/chat.component';
import {CoreComponent} from '../../shared/containers/layout/core/core.component';


const routes: Routes = [
  {
    path: '', component: CoreComponent, children: [
      {
        path: 'restaurant/:id',
        component: RestaurantComponent,
        resolve: {resolvedData: RestaurantViewResolver},
        canActivate: [AuthGuard],
      }
    ]
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRouting {
}
