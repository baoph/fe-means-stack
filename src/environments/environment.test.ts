import { version } from './common';

export const environment = {
  name: 'test',
  production: true,
  debug: false,
  version,
  devProxy: '',
};
