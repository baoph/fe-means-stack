import { version } from './common';

export const environment = {
  name: 'prod',
  production: true,
  debug: false,
  version,
  devProxy: '',
};
