// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
import { version } from './common';

export const environment = {
  name: 'dev',
  production: false,
  debug: true,
  version,
  /**
   * Proxy all request to provided URL (Dev server only)
   *
   * Used only for Dev Server, so even if you set the value in ".prod" or ".test" configuration file,
   * it will not have any effect.
   *
   * See "api-http.interceptor" for the details
   */

  // devProxy: 'http://localhost:80',
  devProxy: 'http://localhost:80',
};
